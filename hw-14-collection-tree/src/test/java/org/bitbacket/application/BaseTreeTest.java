package org.bitbacket.application;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.NoSuchElementException;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class BaseTreeTest {

    private final String name;

    private final ITree actualTree;

    public BaseTreeTest(String name, ITree actualTree) {
        this.name = name;
        this.actualTree = actualTree;
    }

    @Before
    public void setUp() {
        this.actualTree.clear();
    }

    @Parameterized.Parameters(name = "{index} {0}")
    public static Collection<Object[]> instances() {
        return Arrays.asList(new Object[][]{
                {"BaseTree", new BaseTree()},
                {"AVLTree", new AVLTree()}
        });
    }

    @Test
    public void initNull() {
        this.actualTree.init(null);

    }

    @Test
    public void initZero() {
        int[] array = new int[0];
        this.actualTree.init(array);
    }

    @Test
    public void initOne() {
        int[] array = new int[]{1};
        this.actualTree.init(array);
        int[] exp = {1};
        int[] act = this.actualTree.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void initTwo() {
        int[] array = new int[]{1, 2};
        this.actualTree.init(array);
        int[] exp = {1, 2};
        int[] act = this.actualTree.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void initThree() {
        int[] array = new int[]{1, 2, 3};
        this.actualTree.clear();
        this.actualTree.init(array);
        int[] exp = {1, 2, 3};
        int[] act = this.actualTree.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void initMany() {
        int[] array = new int[]{1, 2, 3, 4, 5, 6, 8, 7, 9, 10};
        this.actualTree.init(array);
        int[] exp = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int[] act = this.actualTree.toArray();
        assertArrayEquals(exp, act);
    }
    //=================================================
    //==================== Clear ======================
    //=================================================

    @Test
    public void clearOne() {
        int[] array = new int[]{1};
        this.actualTree.init(array);
        this.actualTree.clear();
        int[] exp = {};
        int[] act = this.actualTree.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void clearTwo() {
        int[] array = new int[]{1, 2};
        this.actualTree.init(array);
        this.actualTree.clear();
        int[] exp = {};
        int[] act = this.actualTree.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void clearThree() {
        int[] array = new int[]{1, 2, 3};
        this.actualTree.init(array);
        this.actualTree.clear();
        int[] exp = {};
        int[] act = this.actualTree.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = RuntimeException.class)
    public void clearMany() {
        int[] array = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        this.actualTree.init(array);
        this.actualTree.clear();
        int[] exp = {};
        int[] act = this.actualTree.toArray();
        assertArrayEquals(exp, act);
    }

    //=================================================
    //=================== Size ========================
    //=================================================

    @Test
    public void sizeZero() {
        int[] array = new int[]{};
        this.actualTree.init(array);
        int exp = 0;
        int act = this.actualTree.size();
        assertEquals(exp, act);
    }

    @Test
    public void sizeOne() {
        int[] array = new int[]{1};
        this.actualTree.init(array);
        int exp = 1;
        int act = this.actualTree.size();
        assertEquals(exp, act);
    }

    @Test
    public void sizeTwo() {
        int[] array = new int[]{1, 2};
        this.actualTree.init(array);
        int exp = 2;
        int act = this.actualTree.size();
        assertEquals(exp, act);
    }

    @Test
    public void sizeFive() {
        int[] array = new int[]{1, 2, 3, 4, 5};
        this.actualTree.init(array);
        int exp = 5;
        int act = this.actualTree.size();
        assertEquals(exp, act);
    }

    @Test(expected = RuntimeException.class)
    public void sizeMany() {
        int[] array = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        this.actualTree.init(array);
        int exp = 10;
        int act = this.actualTree.size();
        assertEquals(exp, act);
    }


    //=================================================
    //=================== Add =========================
    //=================================================

    @Test
    public void addOne() {
        int[] array = new int[0];
        this.actualTree.init(array);
        this.actualTree.add(1);
        int[] exp = {1};
        int[] act = this.actualTree.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addTwo() {
        int[] array = new int[]{2};
        this.actualTree.init(array);
        this.actualTree.add(1);
        int[] exp = {1, 2};
        int[] act = this.actualTree.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addThree() {
        int[] array = new int[]{1, 3};
        this.actualTree.init(array);
        this.actualTree.add(2);
        int[] exp = {1, 2, 3};
        int[] act = this.actualTree.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addMany() {
        int[] array = new int[]{1, 2, 3, 5, 6, 7, 8, 9, 10};
        this.actualTree.init(array);
        this.actualTree.add(4);
        int[] exp = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int[] act = this.actualTree.toArray();
        assertArrayEquals(exp, act);
    }

    //=================================================
    //================== Delete =======================
    //=================================================

    @Test(expected = NoSuchElementException.class)
    public void deleteZero() {
        int[] initialArray = {};
        actualTree.init(initialArray);
        actualTree.delete(1);
    }

    @Test
    public void deleteOne() {
        int[] array = new int[]{1};
        this.actualTree.init(array);
        this.actualTree.delete(1);
        int[] exp = new int[]{};
        int[] act = this.actualTree.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = NoSuchElementException.class)
    public void deleteNoSuchElementOne() {
        int[] array = new int[]{1, 2, 3, 4, 5, 6};
        this.actualTree.init(array);
        this.actualTree.delete(10);
    }

    @Test
    public void deleteSix() {
        int[] array = new int[]{1, 2, 3, 4, 5, 6};
        this.actualTree.init(array);
        this.actualTree.delete(2);
        int[] exp = new int[]{1, 3, 4, 5, 6};
        int[] act = this.actualTree.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void leaf() {
        int[] array = new int[]{2, 1, 23, 43, 12, 54, 44, 13, 56, 34, 11};
        this.actualTree.init(array);
        int exp = 6;
        int act = this.actualTree.leaves();
        assertEquals(exp, act);
    }

    @Test
    public void nodes() {
        int[] array = new int[]{2, 1, 23, 43, 12, 54, 44, 13, 56, 34, 11, 57};
        this.actualTree.init(array);
        int exp = 6;
        int act = this.actualTree.nodes();
        assertEquals(exp, act);
    }

    @Test
    public void height() {
        int[] array = new int[]{1, 24, 30, 2, 81, 3, 5, 4, 6, 7};
        this.actualTree.init(array);
        int exp = 6;
        int act = this.actualTree.height();
        assertEquals(exp, act);
    }

    @Test
    public void width() {
        int[] array = new int[]{67, 4, 48, 86, 78, 73};
        this.actualTree.init(array);
        int exp = 3;
        int act = this.actualTree.width();
        assertEquals(exp, act);
    }

    @Test
    public void reverse() {
        int[] array = new int[]{67, 4, 48, 86, 78, 73};
        this.actualTree.init(array);
        this.actualTree.reverse();
        int[] exp = new int[] {86, 78, 73, 67, 48, 4};
        int[] act = this.actualTree.toArray();
        assertArrayEquals(exp, act);
    }


}