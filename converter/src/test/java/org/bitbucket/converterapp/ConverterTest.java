package org.bitbucket.converterapp;

import org.bitbucket.converterapp.exception.MyException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ConverterTest {

    ConverterWrapper converterWrapper = new ConverterWrapper();

    /*===================================== Usd To Uah ======================================*/

    @Test
    public void usdToUahZero() {
        String exp = "0.0 USD = 0.0 UAH";
        String act = this.converterWrapper.convert(0, "USD", 0);
        assertEquals(exp, act);
    }

    @Test
    public void usdToUahOne() {
        String exp = "1.0 USD = 27.0 UAH";
        String act = this.converterWrapper.convert(1, "USD", 0);
        assertEquals(exp, act);
    }

    @Test
    public void usdToUahTwo() {
        String exp = "2.0 USD = 54.0 UAH";
        String act = this.converterWrapper.convert(2, "USD", 0);
        assertEquals(exp, act);
    }

    @Test
    public void usdToUahMany() {
        String exp = "123123.0 USD = 3324321.0 UAH";
        String act = this.converterWrapper.convert(123123, "USD", 0);
        assertEquals(exp, act);
    }

    @Test
    public void usdToUahCommissionOne() {
        String exp = "10.0 USD = 261.9 UAH";
        String act = this.converterWrapper.convert(10, "USD", 0.03);
        assertEquals(exp, act);
    }

    @Test
    public void usdToUahCommissionTwo() {
        String exp = "10.0 USD = 256.5 UAH";
        String act = this.converterWrapper.convert(10, "USD", 0.05);
        assertEquals(exp, act);
    }

    @Test
    public void usdToUahIgnoreCase() {
        String exp = "10.0 USD = 256.5 UAH";
        String act = this.converterWrapper.convert(10, "usd", 0.05);
        assertEquals(exp, act);
    }

    @Test
    public void usdToUahIgnoreCaseOne() {
        String exp = "10.0 USD = 256.5 UAH";
        String act = this.converterWrapper.convert(10, "Usd", 0.05);
        assertEquals(exp, act);
    }

    @Test(expected = MyException.class)
    public void usdToUahCommissionMany() {
        String act = this.converterWrapper.convert(10, "USD", 1);
    }

    @Test(expected = MyException.class)
    public void usdToUahCommissionOdd() {
        String act = this.converterWrapper.convert(10, "USD", -1);
    }

    @Test(expected = MyException.class)
    public void usdToUahCommissionMore() {
        String act = this.converterWrapper.convert(0, "USD", 0.9);
    }

    @Test(expected = MyException.class)
    public void usdToUahOdd() {
        String act = this.converterWrapper.convert(-10, "USD", 0.05);
    }

    @Test(expected = MyException.class)
    public void usdToUahCurrency() {
        String act = this.converterWrapper.convert(10, "RUS", 0.05);
    }

    /*===================================== Uah To Usd ======================================*/

    @Test
    public void uahToUsdZero() {
        String exp = "0.0 UAH = 0.0 USD";
        String act = this.converterWrapper.convert(0, "UAH", 0);
        assertEquals(exp, act);
    }

    @Test
    public void uahToUsdOne() {
        String exp = "1.0 UAH = 0.037 USD";
        String act = this.converterWrapper.convert(1, "UAH", 0);
        assertEquals(exp, act);
    }

    @Test
    public void uahToUsdTwo() {
        String exp = "2.0 UAH = 0.074 USD";
        String act = this.converterWrapper.convert(2, "UAH", 0);
        assertEquals(exp, act);
    }

    @Test
    public void uahToUsdMany() {
        String exp = "123123.0 UAH = 4555.5509999999995 USD";
        String act = this.converterWrapper.convert(123123, "UAH", 0);
        assertEquals(exp, act);
    }

    @Test
    public void uahToUsdCommissionOne() {
        String exp = "27000.0 UAH = 989.01 USD";
        String act = this.converterWrapper.convert(27000, "UAH", 0.01);
        assertEquals(exp, act);
    }

    @Test
    public void uahToUsdCommissionTwo() {
        String exp = "27000.0 UAH = 949.05 USD";
        String act = this.converterWrapper.convert(27000, "UAH", 0.05);
        assertEquals(exp, act);
    }

    @Test
    public void uahToUsdIgnoreCase() {
        String exp = "27000.0 UAH = 949.05 USD";
        String act = this.converterWrapper.convert(27000, "uah", 0.05);
        assertEquals(exp, act);
    }

    @Test
    public void uahToUsdIgnoreCaseOne() {
        String exp = "27000.0 UAH = 949.05 USD";
        String act = this.converterWrapper.convert(27000, "Uah", 0.05);
        assertEquals(exp, act);
    }

    @Test(expected = MyException.class)
    public void uahToUsdCommissionMany() {
        String act = this.converterWrapper.convert(10, "UAH", 1);
    }

    @Test(expected = MyException.class)
    public void uahToUsdCommissionOdd() {
        String act = this.converterWrapper.convert(10, "UAH", -1);
    }

    @Test(expected = MyException.class)
    public void uahToUsdCommissionMore() {
        String act = this.converterWrapper.convert(0, "UAH", 0.9);
    }

    @Test(expected = MyException.class)
    public void uahToUsdOdd() {
        String act = this.converterWrapper.convert(-10, "UAH", 0.05);
    }

    @Test(expected = MyException.class)
    public void uahToUsdCurrency() {
        String act = this.converterWrapper.convert(10, "RUS", 0.05);
    }

}