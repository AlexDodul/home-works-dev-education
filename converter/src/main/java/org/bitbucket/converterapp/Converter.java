package org.bitbucket.converterapp;

import org.bitbucket.converterapp.exception.MyException;

import static org.bitbucket.converterapp.CourseProvider.*;

public class Converter {

    public double usdToUah (double sum, double commission){
        if (sum < 0) {
            throw new MyException("Amount less than 0");
        }
        if (commission > sum) {
            throw new MyException("Commission is more than the amount");
        }
        if (commission >= 1){
            throw new MyException("The commission is very large");
        }
        if (commission < 0){
            throw new MyException("The commission less than 0");
        }
        return sum * getCourseUah() - (sum * getCourseUah()) * commission;
    }

    public double uahToUsd (double sum, double commission){
        if (sum < 0) {
            throw new MyException("Amount less than 0");
        }
        if (commission > sum) {
            throw new MyException("commission is more than the amount");
        }
        if (commission >= 1){
            throw new MyException("The commission is very large");
        }
        if (commission < 0){
            throw new MyException("The commission less than 0");
        }
        return sum * getCourseUsd() - (sum * getCourseUsd()) * commission;
    }
}
