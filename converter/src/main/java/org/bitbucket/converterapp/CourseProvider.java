package org.bitbucket.converterapp;

public class CourseProvider {

    private static final double COURSE_UAH = 27;

    private static final double COURSE_USD = 0.037;

    private static final String UAH = "UAH";

    private static final String USD = "USD";

    public static double getCourseUah() {
        return COURSE_UAH;
    }

    public static double getCourseUsd() {
        return COURSE_USD;
    }

    public static String getUAH() {
        return UAH;
    }

    public static String getUSD() {
        return USD;
    }
}
