package org.bitbucket.converterapp;

import org.bitbucket.converterapp.exception.MyException;

import static org.bitbucket.converterapp.CourseProvider.*;

public class ConverterWrapper {

    Converter converter = new Converter();

    public double result = 0;

    public String convert(double sum, String currency, double commission) {
        if (currency.equalsIgnoreCase(getUAH())) {
            result = converter.uahToUsd(sum, commission);
            return sum + " UAH = " + result + " USD";
        } else if (currency.equalsIgnoreCase(getUSD())) {
            result = converter.usdToUah(sum, commission);
            return sum + " USD = " + result + " UAH";
        } else {
            throw new MyException("Do not exchange such currency");
        }
    }
}
