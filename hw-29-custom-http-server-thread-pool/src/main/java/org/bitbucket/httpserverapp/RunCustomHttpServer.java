package org.bitbucket.httpserverapp;

public class RunCustomHttpServer {

    public static void main(String[] args) {
        HttpHandler socketHandler = new HttpHandler(8082);
        Thread socketThread = new Thread(socketHandler, "Server");
        socketThread.start();
        System.out.println(socketThread.getName() + " run.");
    }
}
