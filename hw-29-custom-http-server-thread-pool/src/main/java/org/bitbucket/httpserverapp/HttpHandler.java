package org.bitbucket.httpserverapp;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class HttpHandler implements Runnable {

    private boolean isRunning = true;

    private int port;

    private ServerSocket serverSocket;

    public HttpHandler(int port) {
        this.port = port;
    }

    private void executeMethod(SimpleHttpRequest request, SimpleHttpResponse response) throws IOException {
        switch (request.getMethod()) {
            case "GET":
                this.doGet(request, response);
                break;
            case "HEAD":
                this.doHead(request, response);
                break;
            case "POST":
                this.doPost(request, response);
                break;
            case "PUT":
                this.doPut(request, response);
                break;
            case "DELETE":
                this.doDelete(request, response);
                break;
            case "CONNECT":
                this.doConnect(request, response);
                break;
            case "OPTIONS":
                this.doOptions(request, response);
                break;
            case "TRACE":
                this.doTrace(request, response);
                break;
            case "PATCH":
                this.doPatch(request, response);
                break;
            default:
                throw new IOException("Method " + request.getMethod() + " is allowed.");
        }
    }

    public void doGet(SimpleHttpRequest request, SimpleHttpResponse response) {
        String outString = "<h1>Get method was called.</h1>\n <img src=http://helpset.ru/wp-content/uploads/2014/11/20.jpg alt=Picture>\n";
        response.setProtocol("HTTP/1.1");
        response.setStatusCode(200);
        response.setStatusText("OK");
        response.addHeader("Content-Length", "" + outString.length());
        response.addHeader("Content-Type", "text/html");
        response.setBody(outString);
    }

    public void doHead(SimpleHttpRequest request, SimpleHttpResponse response) {
        String outString = "<h1>Head method was called.</h1>\n";
        response.setProtocol("HTTP/1.1");
        response.setStatusCode(200);
        response.setStatusText("OK");
        response.addHeader("Content-Length", "" + outString.length());
        response.addHeader("Content-Type", "text/html");
    }

    public void doPost(SimpleHttpRequest request, SimpleHttpResponse response) {
        String outString = "<h1>Post method was called.</h1>\n";
        response.setProtocol("HTTP/1.1");
        response.setStatusCode(200);
        response.setStatusText("OK");
        response.addHeader("Content-Length", "" + outString.length());
        response.addHeader("Content-Type", "text/html");
        response.setBody(outString);
    }

    public void doPut(SimpleHttpRequest request, SimpleHttpResponse response) {
        String outString = "<h1>Put method was called.</h1>\n";
        response.setProtocol("HTTP/1.1");
        response.setStatusCode(200);
        response.setStatusText("OK");
        response.addHeader("Content-Length", "" + outString.length());
        response.addHeader("Content-Type", "text/html");
        response.setBody(outString);
    }

    public void doDelete(SimpleHttpRequest request, SimpleHttpResponse response) {
        String outString = "<h1>Delete method was called.</h1>\n";
        response.setProtocol("HTTP/1.1");
        response.setStatusCode(200);
        response.setStatusText("OK");
        response.addHeader("Content-Length", "" + outString.length());
        response.addHeader("Content-Type", "text/html");
        response.setBody(outString);
    }

    public void doConnect(SimpleHttpRequest request, SimpleHttpResponse response) {
        String outString = "<h1>Connect method was called.</h1>\n";
        response.setProtocol("HTTP/1.1");
        response.setStatusCode(200);
        response.setStatusText("OK");
        response.addHeader("Content-Length", "" + outString.length());
        response.addHeader("Content-Type", "text/html");
        response.setBody(outString);
    }

    public void doOptions(SimpleHttpRequest request, SimpleHttpResponse response) {
        String outString = "<h1>Options method was called.</h1>\n";
        response.setProtocol("HTTP/1.1");
        response.setStatusCode(200);
        response.setStatusText("OK");
        response.addHeader("Content-Length", "" + outString.length());
        response.addHeader("Content-Type", "text/html");
        response.setBody(outString);
    }

    public void doTrace(SimpleHttpRequest request, SimpleHttpResponse response) {
        String outString = "<h1>Trace method was called.</h1>\n";
        response.setProtocol("HTTP/1.1");
        response.setStatusCode(200);
        response.setStatusText("OK");
        response.addHeader("Content-Length", "" + outString.length());
        response.addHeader("Content-Type", "text/html");
        response.setBody(outString);
    }

    public void doPatch(SimpleHttpRequest request, SimpleHttpResponse response) {
        String outString = "<h1>Patch method was called.</h1>\n";
        response.setProtocol("HTTP/1.1");
        response.setStatusCode(200);
        response.setStatusText("OK");
        response.addHeader("Content-Length", "" + outString.length());
        response.addHeader("Content-Type", "text/html");
        response.setBody(outString);
    }

    public void run() {
        try {
            this.serverSocket = new ServerSocket(this.port);
        } catch (IOException e) {
            try {
                throw new IOException("Failed to open port " + port);
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
        while (this.isRunning) {
            try {
                Socket socket = serverSocket.accept();
                SimpleHttpRequest request = new SimpleHttpRequest();
                SimpleHttpResponse response = new SimpleHttpResponse();
                request.takeRequest(socket);
                this.executeMethod(request, response);
                response.sendResponse(socket);
            } catch (IOException ignore) {
            }
        }
    }
}
