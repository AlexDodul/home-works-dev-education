package com.git.homeworks.Functions;

public class AllTasks {
    public static void main(String[] args) {
        System.out.println(daysOfWeek(5));
        System.out.println(numbersInWords999(55));
        System.out.println(pointDistance(0, 15, 1, 10));
        System.out.println(wordToNumber999BillionFinal("триста тысяч один"));
    }

    //============== Task 1 DaysOfWeek =======================
    public static String daysOfWeek(int dayNumber) {
        if (dayNumber == 1) {
            return "Понедельник";
        } else if (dayNumber == 2) {
            return "Вторник";
        } else if (dayNumber == 3) {
            return "Среда";
        } else if (dayNumber == 4) {
            return "Четверг";
        } else if (dayNumber == 5) {
            return "Пятница";
        } else if (dayNumber == 6) {
            return "Суббота";
        } else if (dayNumber == 7) {
            return "Восскресенье";
        } else {
            return "Неверное число";
        }
    }

    //============== Task 2 NumbersInWords999 =======================
    public static String numbersInWords999(int number) {
        String[] FROM_ZERO_TO_TWENTY = {"ноль", "один", "два", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять", "десять", "одиннадцать", "двенадцать", "тринадцать", "четырнадцать", "пятнадцать", "шестнадцать", "семнадцать", "восемнадцать", "девятнадцать"};
        String[] TEN = {"сто", "десять", "двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят", "семьдесят", "восемьдесят", "девяносто"};
        String[] HUNDREDS = {"тысяча", "сто", "двести", "триста", "четыреста", "пятьсот", "шестьсот", "семьсот", "восемьсот", "девятьсот"};
        String s;

        if (number % 100 < 20 && number != 100) {
            s = FROM_ZERO_TO_TWENTY[number % 100];
            number /= 100;
        } else {
            if (number == 100) {
                return (TEN[0]);
            }
            s = FROM_ZERO_TO_TWENTY[number % 10];
            number /= 10;

            s = TEN[number % 10] + " " + s;
            number /= 10;
        }

        if (number == 0) return s;
        return HUNDREDS[number] + " " + s;
    }

    //============== Task 3 PointDistance =======================
    public static int pointDistance(int x1, int y1, int x2, int y2) {
        return (int) Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2));
    }

    //============== Task 4 WordToNumber999BillionFinal =======================
    public static Long wordToNumber999BillionFinal(String word) {

        long result = 0;
        long finalResult = 0;

        if (word != null && word.length() > 0) {
            String[] splitWord = word.split(" ");

            for (String str : splitWord) {
                if (str.equalsIgnoreCase("ноль")) {
                    result += 0;
                } else if (str.equalsIgnoreCase("один")) {
                    result += 1;
                } else if (str.equalsIgnoreCase("одна")) {
                    result += 1;
                } else if (str.equalsIgnoreCase("два")) {
                    result += 2;
                } else if (str.equalsIgnoreCase("две")) {
                    result += 2;
                } else if (str.equalsIgnoreCase("три")) {
                    result += 3;
                } else if (str.equalsIgnoreCase("четыре")) {
                    result += 4;
                } else if (str.equalsIgnoreCase("пять")) {
                    result += 5;
                } else if (str.equalsIgnoreCase("шесть")) {
                    result += 6;
                } else if (str.equalsIgnoreCase("семь")) {
                    result += 7;
                } else if (str.equalsIgnoreCase("восемь")) {
                    result += 8;
                } else if (str.equalsIgnoreCase("девять")) {
                    result += 9;
                } else if (str.equalsIgnoreCase("десять")) {
                    result += 10;
                } else if (str.equalsIgnoreCase("одиннадцать")) {
                    result += 11;
                } else if (str.equalsIgnoreCase("двенадцать")) {
                    result += 12;
                } else if (str.equalsIgnoreCase("тринадцать")) {
                    result += 13;
                } else if (str.equalsIgnoreCase("четырнадцать")) {
                    result += 14;
                } else if (str.equalsIgnoreCase("пятнадцать")) {
                    result += 15;
                } else if (str.equalsIgnoreCase("шестнадцать")) {
                    result += 16;
                } else if (str.equalsIgnoreCase("семнадцать")) {
                    result += 17;
                } else if (str.equalsIgnoreCase("восемнадцать")) {
                    result += 18;
                } else if (str.equalsIgnoreCase("девятнадцать")) {
                    result += 19;
                } else if (str.equalsIgnoreCase("двадцать")) {
                    result += 20;
                } else if (str.equalsIgnoreCase("тридцать")) {
                    result += 30;
                } else if (str.equalsIgnoreCase("сорок")) {
                    result += 40;
                } else if (str.equalsIgnoreCase("пятьдесят")) {
                    result += 50;
                } else if (str.equalsIgnoreCase("шестьдесят")) {
                    result += 60;
                } else if (str.equalsIgnoreCase("семьдесят")) {
                    result += 70;
                } else if (str.equalsIgnoreCase("восемьдесят")) {
                    result += 80;
                } else if (str.equalsIgnoreCase("девяносто")) {
                    result += 90;
                } else if (str.equalsIgnoreCase("сто")) {
                    result += 100;
                } else if (str.equalsIgnoreCase("двести")) {
                    result += 200;
                } else if (str.equalsIgnoreCase("триста")) {
                    result += 300;
                } else if (str.equalsIgnoreCase("четыреста")) {
                    result += 400;
                } else if (str.equalsIgnoreCase("пятьсот")) {
                    result += 500;
                } else if (str.equalsIgnoreCase("шестьсот")) {
                    result += 600;
                } else if (str.equalsIgnoreCase("семьсот")) {
                    result += 700;
                } else if (str.equalsIgnoreCase("восемьсот")) {
                    result += 800;
                } else if (str.equalsIgnoreCase("девятьсот")) {
                    result += 900;
                } else if (str.equalsIgnoreCase("тысяча")) {
                    result *= 1000;
                    finalResult += result;
                    result = 0;
                } else if (str.equalsIgnoreCase("тысячи")) {
                    result *= 1000;
                    finalResult += result;
                    result = 0;
                } else if (str.equalsIgnoreCase("тысяч")) {
                    result *= 1000;
                    finalResult += result;
                    result = 0;
                } else if (str.equalsIgnoreCase("миллион")) {
                    result *= 1000000;
                    finalResult += result;
                    result = 0;
                } else if (str.equalsIgnoreCase("миллиона")) {
                    result *= 1000000;
                    finalResult += result;
                    result = 0;
                } else if (str.equalsIgnoreCase("миллионов")) {
                    result *= 1000000;
                    finalResult += result;
                    result = 0;
                } else if (str.equalsIgnoreCase("миллиард")) {
                    result *= 1000000000;
                    finalResult += result;
                    result = 0;
                } else if (str.equalsIgnoreCase("миллиарда")) {
                    result *= 1000000000;
                    finalResult += result;
                    result = 0;
                } else if (str.equalsIgnoreCase("миллиардов")) {
                    result *= 1000000000;
                    finalResult += result;
                    result = 0;
                }
            }
            finalResult += result;
        }
        return finalResult;
    }
}
