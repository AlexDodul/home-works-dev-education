package com.git.homeworks.Functions;

import java.util.Scanner;

public class NumbersInWords999 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();
        System.out.println(numbersInWord(num));
    }


    private static String numbersInWord(int number) {
        String[] FROM_ZERO_TO_TWENTY = {"ноль", "один", "два", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять", "десять", "одиннадцать", "двенадцать", "тринадцать", "четырнадцать", "пятнадцать", "шестнадцать", "семнадцать", "восемнадцать", "девятнадцать"};
        String[] TEN = {"сто", "десять", "двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят", "семьдесят", "восемьдесят", "девяносто"};
        String[] HUNDREDS = {"тысяча", "сто", "двести", "триста", "четыреста", "пятьсот", "шестьсот", "семьсот", "восемьсот", "девятьсот"};
        String s;

        if (number % 100 < 20 && number != 100){
            s = FROM_ZERO_TO_TWENTY[number % 100];
            number /= 100;
        }
        else {
            if (number == 100) {
                return (TEN[0]);
            }
            s = FROM_ZERO_TO_TWENTY[number % 10];
            number /= 10;

            s = TEN[number % 10] + " " + s;
            number /= 10;
        }

        if (number == 0) return s;
        return HUNDREDS[number] + " " + s;
    }
}
