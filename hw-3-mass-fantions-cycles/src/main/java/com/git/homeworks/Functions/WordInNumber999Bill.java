package com.git.homeworks.Functions;

import java.util.Arrays;
import java.util.List;

public class WordInNumber999Bill {

    public static void main(String[] args) {

        boolean isValidInput = true;
        long result = 0;
        long finalResult = 0;
        List<String> allowedStrings = Arrays.asList
                (
                        "ноль","один","одина","два","две","три","четыре","пять","шесть","семь",
                        "восемь","девять","десять","одиннадцать","двенадцать","тринадцать","четырнадцать",
                        "пятнадцать","шестнадцать","семнадцать","восемнадцать","девятнадцать","двадцать",
                        "тридцать","сорок","пятьдесят","шестьдесят","семьдесят","восемьдесят","девяносто",
                        "сто","двести","триста","четыреста","пятьсот","шестьсот","семьсот","восемьсот","девятьсот",
                        "тысяча","тысячи","тысяч","миллион","миллиона","миллионов","миллиард","миллиарда","миллиардов"
                );

        //String input="один миллиард девятьсот девяносто девять миллионов две тысячи сто тридцать четыре";
        String input="триста тысяч один";

        if(input != null && input.length()> 0)
        {
            String[] splittedParts = input.trim().split("\\s+");

            for(String str : splittedParts)
            {
                if(!allowedStrings.contains(str))
                {
                    isValidInput = false;
                    System.out.println("Invalid word found : "+str);
                    break;
                }
            }
            if(isValidInput)
            {
                for(String str : splittedParts)
                {
                    if(str.equalsIgnoreCase("ноль")) {
                        result += 0;
                    }
                    else if(str.equalsIgnoreCase("один")) {
                        result += 1;
                    }
                    else if(str.equalsIgnoreCase("одина")) {
                        result += 1;
                    }
                    else if(str.equalsIgnoreCase("два")) {
                        result += 2;
                    }
                    else if(str.equalsIgnoreCase("две")) {
                        result += 2;
                    }
                    else if(str.equalsIgnoreCase("три")) {
                        result += 3;
                    }
                    else if(str.equalsIgnoreCase("четыре")) {
                        result += 4;
                    }
                    else if(str.equalsIgnoreCase("пять")) {
                        result += 5;
                    }
                    else if(str.equalsIgnoreCase("шесть")) {
                        result += 6;
                    }
                    else if(str.equalsIgnoreCase("семь")) {
                        result += 7;
                    }
                    else if(str.equalsIgnoreCase("восемь")) {
                        result += 8;
                    }
                    else if(str.equalsIgnoreCase("девять")) {
                        result += 9;
                    }
                    else if(str.equalsIgnoreCase("десять")) {
                        result += 10;
                    }
                    else if(str.equalsIgnoreCase("одиннадцать")) {
                        result += 11;
                    }
                    else if(str.equalsIgnoreCase("двенадцать")) {
                        result += 12;
                    }
                    else if(str.equalsIgnoreCase("тринадцать")) {
                        result += 13;
                    }
                    else if(str.equalsIgnoreCase("четырнадцать")) {
                        result += 14;
                    }
                    else if(str.equalsIgnoreCase("пятнадцать")) {
                        result += 15;
                    }
                    else if(str.equalsIgnoreCase("шестнадцать")) {
                        result += 16;
                    }
                    else if(str.equalsIgnoreCase("семнадцать")) {
                        result += 17;
                    }
                    else if(str.equalsIgnoreCase("восемнадцать")) {
                        result += 18;
                    }
                    else if(str.equalsIgnoreCase("девятнадцать")) {
                        result += 19;
                    }
                    else if(str.equalsIgnoreCase("двадцать")) {
                        result += 20;
                    }
                    else if(str.equalsIgnoreCase("тридцать")) {
                        result += 30;
                    }
                    else if(str.equalsIgnoreCase("сорок")) {
                        result += 40;
                    }
                    else if(str.equalsIgnoreCase("пятьдесят")) {
                        result += 50;
                    }
                    else if(str.equalsIgnoreCase("шестьдесят")) {
                        result += 60;
                    }
                    else if(str.equalsIgnoreCase("семьдесят")) {
                        result += 70;
                    }
                    else if(str.equalsIgnoreCase("восемьдесят")) {
                        result += 80;
                    }
                    else if(str.equalsIgnoreCase("девяносто")) {
                        result += 90;
                    }
                    else if(str.equalsIgnoreCase("сто")) {
                        result += 100;
                    }

                    else if(str.equalsIgnoreCase("двести")) {
                        result += 200;
                    }else if(str.equalsIgnoreCase("триста")) {
                        result += 300;
                    }else if(str.equalsIgnoreCase("четыреста")) {
                        result += 400;
                    }else if(str.equalsIgnoreCase("пятьсот")) {
                        result += 500;
                    }else if(str.equalsIgnoreCase("шестьсот")) {
                        result += 600;
                    }else if(str.equalsIgnoreCase("семьсот")) {
                        result += 700;
                    }else if(str.equalsIgnoreCase("восемьсот")) {
                        result += 800;
                    }else if(str.equalsIgnoreCase("девятьсот")) {
                        result += 900;
                    }

                    else if(str.equalsIgnoreCase("тысяча")) {
                        result *= 1000;
                        finalResult += result;
                        result=0;
                    }
                    else if(str.equalsIgnoreCase("тысячи")) {
                        result *= 1000;
                        finalResult += result;
                        result=0;
                    }
                    else if(str.equalsIgnoreCase("тысяч")) {
                        result *= 1000;
                        finalResult += result;
                        result=0;
                    }
                    else if(str.equalsIgnoreCase("миллион")) {
                        result *= 1000000;
                        finalResult += result;
                        result=0;
                    }
                    else if(str.equalsIgnoreCase("миллиона")) {
                        result *= 1000000;
                        finalResult += result;
                        result=0;
                    }
                    else if(str.equalsIgnoreCase("миллионов")) {
                        result *= 1000000;
                        finalResult += result;
                        result=0;
                    }
                    else if(str.equalsIgnoreCase("миллиард")) {
                        result *= 1000000000;
                        finalResult += result;
                        result=0;
                    }
                    else if(str.equalsIgnoreCase("миллиарда")) {
                        result *= 1000000000;
                        finalResult += result;
                        result=0;
                    }
                    else if(str.equalsIgnoreCase("миллиардов")) {
                        result *= 1000000000;
                        finalResult += result;
                        result=0;
                    }
                }

                finalResult += result;
                result=0;
                System.out.println(finalResult);
            }
        }

    }

}
