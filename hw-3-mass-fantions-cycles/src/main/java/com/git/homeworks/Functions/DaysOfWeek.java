package com.git.homeworks.Functions;

import java.util.Scanner;

public class DaysOfWeek {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число от 1 до 7");
        int dayNumber = scanner.nextInt();

        if (dayNumber == 1) {
            System.out.println("Сегодня - Понедельник");
        }
        else if (dayNumber == 2) {
            System.out.println("Сегодня - Вторник");
        }
        else if (dayNumber == 3) {
            System.out.println("Сегодня - Среда");
        }
        else if (dayNumber == 4) {
            System.out.println("Сегодня - Четверг");
        }
        else if (dayNumber == 5) {
            System.out.println("Сегодня - Пятница");
        }
        else if (dayNumber == 6) {
            System.out.println("Сегодня - Суббота");
        }
        else if (dayNumber == 7) {
            System.out.println("Сегодня - Восскресенье");
        }
        else {
            System.out.println("Неверное число");
        }

    }

}
