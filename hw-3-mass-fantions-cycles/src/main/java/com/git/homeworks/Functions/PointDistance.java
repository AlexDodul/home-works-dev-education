package com.git.homeworks.Functions;

public class PointDistance {
    public static void main(String[] args) {
        int x1 = 0;
        int y1 = 15;

        int x2 = 1;
        int y2 = 10;

        double a = Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2));

        System.out.println(a);
    }
}
