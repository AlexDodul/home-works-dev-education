package com.git.homeworks.Functions;

import org.junit.Assert;
import org.junit.Test;

public class AllTasksTest {
    //============== Task 1 DaysOfWeek =======================
    @Test
    public void daysOfWeek() {
        String exp = "Пятница";
        String act = AllTasks.daysOfWeek(5);
        Assert.assertEquals(act, exp);
    }

    //============== Task 2 NumbersInWords999 =======================
    @Test
    public void numbersInWords999() {
        String exp = "пятьдесят пять";
        String act = AllTasks.numbersInWords999(55);
        Assert.assertEquals(act, exp);
    }

    //============== Task 3 PointDistance =======================
    @Test
    public void pointDistance() {
        int exp = 5;
        int act = AllTasks.pointDistance(0, 15, 1, 10);
        Assert.assertEquals(act, exp);
    }

    //============== Task 4 WordToNumber999BillionFinal =======================
    @Test
    public void wordToNumber999BillionFinal() {
        long exp = 300001;
        long act = AllTasks.wordToNumber999BillionFinal("триста тысяч один");
        Assert.assertEquals(act, exp);
    }
}
