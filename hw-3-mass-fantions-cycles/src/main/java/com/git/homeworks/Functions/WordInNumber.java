package com.git.homeworks.Functions;

import java.util.Scanner;

public class WordInNumber {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число прописью: ");

        String numbStr = scanner.nextLine();

        String[] arrStr = numbStr.split("\\s+");

        if (arrStr.length == 1) {
            toNumbStr1(arrStr[0]);
        } else if (arrStr.length == 2) {
            toNumbStr2(arrStr[0]);
            toNumbStr1(arrStr[1]);
        } else if (arrStr.length == 3) {
            toNumbStr3(arrStr[0]);
            toNumbStr2(arrStr[1]);
            toNumbStr1(arrStr[2]);
        }
    }
    private static void toNumbStr1 (String number) {
        if (number.equals("Один")) {
            System.out.print(1);
        } else if (number.equals("Два")) {
            System.out.print(2);
        } else if (number.equals("Три")) {
            System.out.print(3);
        } else if (number.equals("Четыри")) {
            System.out.print(4);
        } else if (number.equals("Пять")) {
            System.out.print(5);
        } else if (number.equals("Шесть")) {
            System.out.print(6);
        } else if (number.equals("Семь")) {
            System.out.print(7);
        } else if (number.equals("Восемь")) {
            System.out.print(8);
        } else if (number.equals("Девять")) {
            System.out.print(9);
        } else if (number.equals("Ноль")) {
            System.out.print(0);
        } else if (number.equals("Десять")) {
            System.out.print(10);
        } else if (number.equals("Одинадцать")) {
            System.out.print(11);
        } else if (number.equals("Двенадцать")) {
            System.out.print(12);
        } else if (number.equals("Тринадцать")) {
            System.out.print(13);
        } else if (number.equals("Четырнадцать")) {
            System.out.print(14);
        } else if (number.equals("Пятнадцать")) {
            System.out.print(15);
        } else if (number.equals("Шестнадцать")) {
            System.out.print(16);
        } else if (number.equals("Семнадцать")) {
            System.out.print(17);
        } else if (number.equals("Восемнадцать")) {
            System.out.print(18);
        } else if (number.equals("Девятнадцать")) {
            System.out.print(19);
        } else if (number.equals("Двадцать")) {
            System.out.print(20);
        } else if (number.equals("Тридцать")) {
            System.out.print(30);
        } else if (number.equals("Сорок")) {
            System.out.print(40);
        } else if (number.equals("Пятдесят")) {
            System.out.print(50);
        } else if (number.equals("Шестдесят")) {
            System.out.print(60);
        } else if (number.equals("Семдесят")) {
            System.out.print(70);
        } else if (number.equals("Восемдесят")) {
            System.out.print(80);
        } else if (number.equals("Девяносто")) {
            System.out.print(90);
        } else if (number.equals("Сто")) {
            System.out.print(100);
        } else if (number.equals("Двести")) {
            System.out.print(200);
        } else if (number.equals("Триста")) {
            System.out.print(300);
        } else if (number.equals("Четыриста")) {
            System.out.print(400);
        } else if (number.equals("Пятсот")) {
            System.out.print(500);
        } else if (number.equals("Шестсот")) {
            System.out.print(600);
        } else if (number.equals("Семсот")) {
            System.out.print(700);
        } else if (number.equals("Восемсот")) {
            System.out.print(800);
        } else if (number.equals("Девятсот")) {
            System.out.print(900);
        } else {
            System.out.print("Не найдено");
        }
    }

    private static void toNumbStr2 (String number) {

        if (number.equals("Двадцать")) {
            System.out.print(2);
        } else if (number.equals("Тридцать")) {
            System.out.print(3);
        } else if (number.equals("Сорок")) {
            System.out.print(4);
        } else if (number.equals("Пятдесят")) {
            System.out.print(5);
        } else if (number.equals("Шестдесят")) {
            System.out.print(6);
        } else if (number.equals("Семдесят")) {
            System.out.print(7);
        } else if (number.equals("Восемдесят")) {
            System.out.print(8);
        } else if (number.equals("Девяносто")) {
            System.out.print(9);
        } else if (number.equals("Сто")) {
            System.out.print(10);
        } else if (number.equals("Двести")) {
            System.out.print(20);
        } else if (number.equals("Триста")) {
            System.out.print(30);
        } else if (number.equals("Четыриста")) {
            System.out.print(40);
        } else if (number.equals("Пятсот")) {
            System.out.print(50);
        } else if (number.equals("Шестсот")) {
            System.out.print(60);
        } else if (number.equals("Семсот")) {
            System.out.print(70);
        } else if (number.equals("Восемсот")) {
            System.out.print(80);
        } else if (number.equals("Девятсот")) {
            System.out.print(90);
        } else {
            System.out.print("Не найдено");
        }
    }

    private static void toNumbStr3 (String number) {

        if (number.equals("Сто")) {
            System.out.print(1);
        } else if (number.equals("Двести")) {
            System.out.print(2);
        } else if (number.equals("Триста")) {
            System.out.print(3);
        } else if (number.equals("Четыриста")) {
            System.out.print(4);
        } else if (number.equals("Пятсот")) {
            System.out.print(5);
        } else if (number.equals("Шестсот")) {
            System.out.print(6);
        } else if (number.equals("Семсот")) {
            System.out.print(7);
        } else if (number.equals("Восемсот")) {
            System.out.print(8);
        } else if (number.equals("Девятсот")) {
            System.out.print(9);
        } else {
            System.out.print("Не найдено");
        }
    }
}
