package com.git.homeworks.Operators;

import java.util.Scanner;

public class Rating {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите рейтинг студента");
        int a = scanner.nextInt();

        if (a >= 0 && a <= 19){
            System.out.println("F");
        }
        else if (a >= 20 && a <= 39){
            System.out.println("E");
        }
        else if (a >= 40 && a <= 59){
            System.out.println("D");
        }
        else if (a >= 60 && a <= 74){
            System.out.println("C");
        }
        else if (a >= 75 && a <= 89){
            System.out.println("B");
        }
        else if (a >= 90 && a <= 100){
            System.out.println("A");
        }
    }
}
