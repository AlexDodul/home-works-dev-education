package com.git.homeworks.Operators;

import org.junit.Assert;
import org.junit.Test;

public class AllTasksTest {
    //============== Task 1 =======================
    @Test
    public void evenFirst() {
        int exp = 20;
        int act = AllTasks.even(4, 5);
        Assert.assertEquals(act, exp);
    }

    @Test
    public void evenSecond() {
        int exp = 0;
        int acs = AllTasks.even(-2, 2);
        Assert.assertEquals(acs, exp);
    }

    //============== Task 2 =======================
    @Test
    public void coordinatesFirst() {
        String exp = "Первая четверть";
        String act = AllTasks.coordinates(4, 4);
        Assert.assertEquals(act, exp);
    }

    @Test
    public void coordinatesSecond() {
        String exp = "Не пренадлежит ни какой четверти";
        String acs = AllTasks.coordinates(0, 0);
        Assert.assertEquals(acs, exp);
    }

    //============== Task 3 =======================
    @Test
    public void sumPositiveNumberFirst() {
        int exp = 0;
        int act = AllTasks.sumPositiveNumber(0, 0, 0);
        Assert.assertEquals(act, exp);
    }

    @Test
    public void sumPositiveNumberSecond() {
        int exp = 5;
        int act = AllTasks.sumPositiveNumber(-1, 2, 3);
        Assert.assertEquals(act, exp);
    }

    //============== Task 4 =======================
    @Test
    public void maxFirst() {
        int exp = 6;
        int act = AllTasks.max(1, 1, 1);
        Assert.assertEquals(act, exp);
    }

    @Test
    public void maxSecond() {
        int exp = 11;
        int act = AllTasks.max(2, 2, 2);
        Assert.assertEquals(act, exp);
    }

    //============== Task 5 =======================
    @Test
    public void ratingFirst() {
        String exp = "F";
        String act = AllTasks.rating(19);
        Assert.assertEquals(act, exp);
    }

    @Test
    public void ratingSecond() {
        String exp = "A";
        String act = AllTasks.rating(90);
        Assert.assertEquals(act, exp);
    }
}
