package com.git.homeworks.Operators;

public class AllTasks {
    public static void main(String[] args) {
        System.out.println(even(5, 3));
        System.out.println(coordinates(4, 4));
        System.out.println(sumPositiveNumber(3, 3, 3));
        System.out.println(max(2, 2, 2));
        System.out.println(rating(19));
    }

    //=============Task 1==================
    public static int even(int a, int b) {
        if (a >= 0) {
            return a * b;
        } else {
            return a + b;
        }
    }

    //==============Task 2=================
    public static String coordinates(int x, int y) {
        if (x > 0 && y > 0) {
            return "Первая четверть";
        } else if (x > 0 && y < 0) {
            return "Вторая четверть";
        } else if (x < 0 && y < 0) {
            return "Третья четверть";
        } else if (x < 0 && y > 0) {
            return "Четвертая четверть";
        } else {
            return "Не пренадлежит ни какой четверти";
        }
    }

    //==============Task 3=================
    public static int sumPositiveNumber(int a, int b, int c) {
        if (a >= 0 && b >= 0 && c >= 0) {
            return a + b + c;
        } else if (a < 0 && b >= 0 && c >= 0) {
            return b + c;
        } else if (a >= 0 && b < 0 && c >= 0) {
            return a + c;
        } else if (a >= 0 && b >= 0 && c < 0) {
            return a + b;
        } else if (a >= 0 && b < 0 && c < 0) {
            return a;
        } else if (a < 0 && b >= 0 && c < 0) {
            return b;
        } else if (a < 0 && b < 0 && c >= 0) {
            return c;
        } else {
            return 0;
        }
    }

    //==============Task 4=================
    public static int max(int a, int b, int c) {
        int multiply = a * b * c;
        int sum = a + b + c;

        if (multiply > sum) {
            return (multiply + 3);
        } else if (multiply < sum) {
            return (sum + 3);
        } else {
            return (sum + 3);
        }
    }

    //==============Task 5=================
    public static String rating(int a) {
        if (a >= 0 && a <= 19) {
            return "F";
        } else if (a >= 20 && a <= 39) {
            return "E";
        } else if (a >= 40 && a <= 59) {
            return "D";
        } else if (a >= 60 && a <= 74) {
            return "C";
        } else if (a >= 75 && a <= 89) {
            return "B";
        } else if (a >= 90 && a <= 100) {
            return "A";
        } else {
            return "Число не входит в рейтинг";
        }
    }
}
