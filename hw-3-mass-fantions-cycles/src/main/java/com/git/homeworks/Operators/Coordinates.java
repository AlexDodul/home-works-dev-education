package com.git.homeworks.Operators;

public class Coordinates {
    public static void main(String[] args) {
        int x = -1;
        int y = 2;

        if (x == 0 && y == 0){
            System.out.println("Не пренадлежит ни какой четверти");
        }
        else if (x > 0 && y > 0){
            System.out.println("Первая четверть");
        }
        else if (x > 0 && y < 0){
            System.out.println("Вторая четверть");
        }
        else if (x < 0 && y < 0){
            System.out.println("Третья четверть");
        }
        else if (x < 0 && y > 0){
            System.out.println("Четвертая четверть");
        }
    }
}
