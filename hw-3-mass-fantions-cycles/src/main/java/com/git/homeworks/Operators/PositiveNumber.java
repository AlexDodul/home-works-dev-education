package com.git.homeworks.Operators;

public class PositiveNumber {
    public static void main(String[] args) {
        int a = 1;
        int b = -2;
        int c = 3;
        int result = 0;

        if (a >= 0 && b >= 0 && c >=0){
            result = a + b + c;
        }
        else if(a < 0 && b >= 0 && c >=0){
            result = b + c;
        }
        else if(a >= 0 && b < 0 && c >=0){
            result = a + c;
        }
        else if(a >= 0 && b >= 0 && c < 0){
            result = a + b;
        }
        else if(a >= 0 && b < 0 && c < 0){
            result = a;
        }
        else if(a < 0 && b >= 0 && c < 0){
            result = b;
        }
        else if(a < 0 && b < 0 && c >= 0){
            result = c;
        }
        else {
            System.out.println("Положительных чисел нет");
        }
        System.out.println(result);
    }
}
