package com.git.homeworks.Cycles;

import java.util.Scanner;

public class ReverseNumber {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите число - ");
        int a = scanner.nextInt();
        int b = 0;
        int reverseNum = 0;

        while (a > 0) {
            b = a % 10;
            a = a / 10;
            if (a > 0) {
                reverseNum = (reverseNum + b) * 10;
            } else {
                reverseNum = (reverseNum + b);
            }
        }
        System.out.println("Результат = " + reverseNum);
    }
}
