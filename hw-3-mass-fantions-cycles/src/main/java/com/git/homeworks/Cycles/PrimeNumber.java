package com.git.homeworks.Cycles;

import java.util.Scanner;

public class PrimeNumber {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Введите число - ");
        int a = scan.nextInt();

        int count = 0;

        if (a == 1){
            System.out.println("Число " + a + " не является простым");
        }

        else if (a == 2){
            System.out.println( a + " - простое число");
        }
        else {
            for (int i = 2; i <= a/2; i++) {
                if (a % i == 0){
                    count++;
                }
            }
            if (count > 0){
                System.out.println("Число " + a + " не является простым");
            }
            else {
                System.out.println( a + " - простое число");
            }
        }
    }
}
