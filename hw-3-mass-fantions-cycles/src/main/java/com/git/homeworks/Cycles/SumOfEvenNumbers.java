package com.git.homeworks.Cycles;

public class SumOfEvenNumbers {
    public static void main(String[] args) {
        int count = 0;
        int sum = 0;

        for (int i = 1; i < 100; i++) {
            if (i % 2 == 0){
                sum += i;
                count++;
            }
        }
        System.out.println("Сумма положительных чисет = " + sum);
        System.out.println("Кол-во положительных чисел = " + count);
    }
}
