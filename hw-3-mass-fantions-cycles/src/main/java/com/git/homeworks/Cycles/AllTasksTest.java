package com.git.homeworks.Cycles;

import org.junit.Assert;
import org.junit.Test;

public class AllTasksTest {

    //============== Task 1 =======================
    @Test
    public void sumOfEvenNumbers() {
        int exp = 2450;
        int act = AllTasks.sumOfEvenNumbers(100);
        Assert.assertEquals(act, exp);

    }

    @Test
    public void countOfEvenNumbers() {
        int exp = 49;
        int act = AllTasks.countOfEvenNumbers(100);
        Assert.assertEquals(act, exp);
    }

    //============== Task 2 =======================
    @Test
    public void primeNumberFirst() {
        String exp = "7 - простое число";
        String act = AllTasks.primeNumber(7);
        Assert.assertEquals(act, exp);
    }

    @Test
    public void primeNumberSecond() {
        String exp = "Число 10 не является простым";
        String act = AllTasks.primeNumber(10);
        Assert.assertEquals(act, exp);
    }

    //============== Task 3 =======================
    @Test
    public void naturalRootFirst() {
        int exp = 5;
        int act = AllTasks.naturalRoot(25);
        Assert.assertEquals(act, exp);
    }

    @Test
    public void naturalRootSecond() {
        int exp = 7;
        int act = AllTasks.naturalRoot(49);
        Assert.assertEquals(act, exp);
    }

    //============== Task 4 =======================
    @Test
    public void factorialFirst() {
        int exp = 120;
        int act = AllTasks.factorial(5);
        Assert.assertEquals(act, exp);
    }

    @Test
    public void factorialSecond() {
        int exp = 720;
        int act = AllTasks.factorial(6);
        Assert.assertEquals(act, exp);
    }

    //============== Task 5 =======================
    @Test
    public void sumOfDigitsFirst() {
        int exp = 6;
        int act = AllTasks.sumOfDigits(123);
        Assert.assertEquals(act, exp);
    }

    @Test
    public void sumOfDigitsSecond() {
        int exp = 10;
        int act = AllTasks.sumOfDigits(1234);
        Assert.assertEquals(act, exp);
    }

    //============== Task 6 =======================
    @Test
    public void reverseNumberFirst() {
        int exp = 321;
        int act = AllTasks.reverseNumber(123);
        Assert.assertEquals(act, exp);
    }

    @Test
    public void reverseNumberSecond() {
        int exp = 4321;
        int act = AllTasks.reverseNumber(1234);
        Assert.assertEquals(act, exp);
    }
}
