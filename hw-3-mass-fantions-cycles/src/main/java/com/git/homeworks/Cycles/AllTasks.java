package com.git.homeworks.Cycles;

import org.w3c.dom.ls.LSOutput;

public class AllTasks {

    public static void main(String[] args) {
        System.out.println(sumOfEvenNumbers(100));
        System.out.println(countOfEvenNumbers(100));
        System.out.println(primeNumber(7));
        System.out.println(naturalRoot(25));
        System.out.println(factorial(5));
        System.out.println(sumOfDigits(1234));
        System.out.println(reverseNumber(123));
    }

    //==============Task 1=================
    public static int sumOfEvenNumbers(int max) {
        int sum = 0;
        for (int i = 1; i < max; i++) {
            if (i % 2 == 0) {
                sum += i;
            }
        }
        return sum;
    }

    public static int countOfEvenNumbers(int max) {
        int count = 0;
        for (int i = 1; i < max; i++) {
            if (i % 2 == 0) {
                count++;
            }
        }
        return count;
    }

    //==============Task 2=================
    public static String primeNumber(int number) {
        int count = 0;

        if (number == 1) {
            return "Число " + number + " не является простым";
        } else if (number == 2) {
            return number + " - простое число";
        } else {
            for (int i = 2; i <= number / 2; i++) {
                if (number % i == 0) {
                    count++;
                }
            }
            if (count > 0) {
                return "Число " + number + " не является простым";
            } else {
                return number + " - простое число";
            }
        }
    }

    //==============Task 3=================
    public static int naturalRoot(int a) {
        int num = 1;
        int res = 0;
        while (a > 0) {
            a = a - num;
            num = num + 2;
            res += (a < 0) ? 0 : 1;
        }
        return res;
    }

    //==============Task 4=================
    public static int factorial(int n) {
        int result = 1;
        for (int i = 1; i <= n; i++) {
            result = result * i;
        }
        return result;
    }

    //==============Task 5=================
    public static int sumOfDigits(int a) {
        int b = 0;
        int sum = 0;

        while (a > 0) {
            b = a % 10;
            a = a / 10;
            sum = sum + b;
        }
        return sum;
    }

    //==============Task 6=================
    public static int reverseNumber(int a) {
        int b = 0;
        int reverseNum = 0;

        while (a > 0) {
            b = a % 10;
            a = a / 10;
            if (a > 0) {
                reverseNum = (reverseNum + b) * 10;
            } else {
                reverseNum = (reverseNum + b);
            }
        }
        return reverseNum;
    }
}
