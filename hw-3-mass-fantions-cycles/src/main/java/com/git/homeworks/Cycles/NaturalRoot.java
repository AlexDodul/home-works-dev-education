package com.git.homeworks.Cycles;

import java.util.Scanner;

public class NaturalRoot {
    public static void main(String[] args) {

        Scanner scann = new Scanner(System.in);
        System.out.print("Введите число - ");
        int a = scann.nextInt();

        //int numSqrt = (int)Math.sqrt(a);
        //System.out.println("Корень числа " + a + " = " + numSqrt);

        int num = 1;
        int res = 0;
        while(a > 0){
            a = a - num;
            num = num + 2;
            res += (a < 0) ? 0:1;
        }
        System.out.println(res);
    }
}
