package com.git.homeworks.Cycles;

import java.util.Scanner;

public class SumOfDigits {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите число - ");
        int a = scanner.nextInt();
        int b = 0;
        int sum = 0;

        while (a > 0){
            b = a % 10;
            a = a / 10;
            sum = sum + b;
        }
        System.out.println("Сумма чисел = " + sum);
    }
}
