package com.git.homeworks.Arrays;

public class SumOfOddElements {
    public static void main(String[] args) {
        int [] arr = new int[] {15, 20, 35, -2, 5, 11, 22, -9, 1};
        int sum = 0;

        for (int i = 0; i < arr.length; i++) {
            if (i % 2 != 0){
                sum += arr[i];
            }
        }
        System.out.println(sum);
    }
}
