package com.git.homeworks.Arrays;

public class MinimumIndex {
    public static void main(String[] args) {
        int [] arr = new int[] {15, 20, 35, -2, 5, 11, 22, 4, 1};
        int min = arr[0];
        int index = 0;

        for (int i = 0; i < arr.length; i++) {
            if (min > arr[i]){
                min = arr[i];
                index = i;
            }
        }
        System.out.println(index);
    }
}
