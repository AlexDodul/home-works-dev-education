package com.git.homeworks.Arrays;

public class MaximumIndex {
    public static void main(String[] args) {
        int [] arr = new int[] {15, 20, -35, -2, 5, 11, 22, 4, 1};
        int max = arr[0];
        int index = 0;

        for (int i = 0; i < arr.length; i++) {
            if (max < arr[i]){
                max = arr[i];
                index = i;
            }
        }
        System.out.println(index);
    }
}
