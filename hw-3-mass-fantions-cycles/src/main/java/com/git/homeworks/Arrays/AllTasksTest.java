package com.git.homeworks.Arrays;

import org.junit.Assert;
import org.junit.Test;

public class AllTasksTest {

    //============== Task 1 =======================
    @Test
    public void minimumElementFirst() {
        int exp = -2;
        int[] data = new int[]{5, 10, 8, 0, -2, 7, 3};
        int act = AllTasks.minimumElement(data);
        Assert.assertEquals(act, exp);
    }

    @Test
    public void minimumElementSecond() {
        int exp = 0;
        int[] data = new int[]{5, 10, 8, 0, 2, 7, 3};
        int act = AllTasks.minimumElement(data);
        Assert.assertEquals(act, exp);
    }

    //============== Task 2 =======================
    @Test
    public void maximumElementFirst() {
        int exp = 10;
        int[] data = new int[]{5, 10, 8, 0, -2, 7, 3};
        int act = AllTasks.maximumElement(data);
        Assert.assertEquals(act, exp);
    }

    @Test
    public void maximumElementSecond() {
        int exp = 70;
        int[] data = new int[]{5, 10, 8, 0, 2, 70, 3};
        int act = AllTasks.maximumElement(data);
        Assert.assertEquals(act, exp);
    }

    //============== Task 3 =======================
    @Test
    public void minimumIndexFirst() {
        int exp = 4;
        int[] data = new int[]{5, 10, 8, 0, -2, 7, 3};
        int act = AllTasks.minimumIndex(data);
        Assert.assertEquals(act, exp);
    }

    @Test
    public void minimumIndexSecond() {
        int exp = 3;
        int[] data = new int[]{5, 10, 8, 0, 2, 7, 3};
        int act = AllTasks.minimumIndex(data);
        Assert.assertEquals(act, exp);
    }

    //============== Task 4 =======================
    @Test
    public void maximumIndexFirst() {
        int exp = 1;
        int[] data = new int[]{5, 10, 8, 0, -2, 7, 3};
        int act = AllTasks.maximumIndex(data);
        Assert.assertEquals(act, exp);
    }

    @Test
    public void maximumIndexSecond() {
        int exp = 2;
        int[] data = new int[]{5, 0, 8, 0, 2, 7, 3};
        int act = AllTasks.maximumIndex(data);
        Assert.assertEquals(act, exp);
    }

    //============== Task 5 =======================
    @Test
    public void sumOfOddElementsFirst() {
        int exp = 17;
        int[] data = new int[]{5, 10, 8, 0, -2, 7, 3};
        int act = AllTasks.sumOfOddElements(data);
        Assert.assertEquals(act, exp);
    }

    @Test
    public void sumOfOddElementsSecond() {
        int exp = 5;
        int[] data = new int[]{5, 0, 8, 0, 2, 5, 3};
        int act = AllTasks.sumOfOddElements(data);
        Assert.assertEquals(act, exp);
    }

    //============== Task 6 =======================
    @Test
    public void reverseArrayFirst() {
        int[] exp = new int[]{7, 6, 5, 4, 3, 2, 1};
        int[] data = new int[]{1, 2, 3, 4, 5, 6, 7};
        int[] act = AllTasks.reverseArray(data);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void reverseArraySecond() {
        int[] exp = new int[]{9, 8, 7, 6, 5, 4, 3, 2, 1};
        int[] data = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
        int[] act = AllTasks.reverseArray(data);
        Assert.assertArrayEquals(exp, act);
    }

    //============== Task 7 =======================
    @Test
    public void numberOfOddElementsFirst() {
        int exp = 4;
        int[] data = new int[]{1, 2, 3, 4, 5, 6, 7};
        int act = AllTasks.numberOfOddElements(data);
        Assert.assertEquals(act, exp);
    }

    @Test
    public void numberOfOddElementsSecond() {
        int exp = 1;
        int[] data = new int[]{5, 0};
        int act = AllTasks.numberOfOddElements(data);
        Assert.assertEquals(act, exp);
    }

    //============== Task 8 =======================
    @Test
    public void changeArrFirst() {
        int[] exp = new int[]{5, 6, 7, 8, 1, 2, 3, 4};
        int[] data = new int[]{1, 2, 3, 4, 5, 6, 7, 8};
        int[] act = AllTasks.changeArr(data);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void changeArrSecond() {
        int[] exp = new int[]{3, 4, 1, 2};
        int[] data = new int[]{1, 2, 3, 4};
        int[] act = AllTasks.changeArr(data);
        Assert.assertArrayEquals(exp, act);
    }

    //============== Task 9 =======================
    @Test
    public void bubbleSort() {
        int[] exp = new int[]{-2, 0, 3, 5, 7, 8, 10};
        int[] data = new int[]{5, 10, 8, 0, -2, 7, 3};
        int[] act = AllTasks.bubbleSort(data);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void insertSort() {
        int[] exp = new int[]{-2, 0, 3, 5, 7, 8, 10};
        int[] data = new int[]{5, 10, 8, 0, -2, 7, 3};
        int[] act = AllTasks.insertSort(data);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void selectSort() {
        int[] exp = new int[]{-2, 0, 3, 5, 7, 8, 10};
        int[] data = new int[]{5, 10, 8, 0, -2, 7, 3};
        int[] act = AllTasks.selectSort(data);
        Assert.assertArrayEquals(exp, act);
    }

    //============== Task 10 =======================
    @Test
    public void shellSort() {
        int[] exp = new int[]{-2, 0, 3, 5, 7, 8, 10};
        int[] data = new int[]{5, 10, 8, 0, -2, 7, 3};
        int[] act = AllTasks.shellSort(data);
        Assert.assertArrayEquals(exp, act);
    }
}
