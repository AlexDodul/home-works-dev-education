package com.git.homeworks.Arrays;

public class SortWithStar {
/*----------------------------- Shell sort ---------------------------------------
    public static void main(String[] args) {
        int[] num = {100, 5, 10, 55, 60, 7, 17, 91, 33};

        shellSort(num);
        for (int i : num)
            System.out.print(i + " ");
    }

    public static void shellSort(int[] numbers){
        int j;
        for( int gap = numbers.length / 2; gap > 0; gap /= 2 ){
            for(int i=gap;i<numbers.length;i++){
                int temp = numbers[i];
                for (j = i; j >= gap && numbers[j - gap] > temp; j -= gap)
                {
                    numbers[j] = numbers[j - gap];
                }
                numbers[j] = temp;
            }
        }
    }
 -------------------------------------------- end Shell sort -------------------------------------------*/

/*--------------------------------------- Quick sort ------------------------------------*/
    /*public static void main(String[] args) {

        int[] num = {100, 5, 10, 55, 60, 7, 17, 91, 33};
        int n = num.length;
        quicksort(num, 0, n-1);

        for (int i = 0; i < n; i++)
            System.out.print(num[i]+ " ");
    }

    public static void quicksort(int[] numbers, int low, int high) {
        if (low < high) {
            int dp = partition(numbers, low, high);
            quicksort(numbers, low, dp-1);
            quicksort(numbers, dp+1, high);
        }
    }

    private static int partition(int[] numbers, int low, int high) {
        int pivot = numbers[low];
        int i = low;
        for (int j = low + 1; j <= high; j++)
            if (numbers[j] < pivot) {
                ++i;
                swap(numbers, i, j);
            }

        swap(numbers, low, i);
        return i;
    }
    private static void swap(int[] list, int i, int j) {
        int temp = list[i];
        list[i] = list[j];
        list[j] = temp;
    }*/
/*----------------------------------------- end Quick sort --------------------*/

/*----------------------------------------- Merge sort --------------------*/

    /*public static void main(String[] args) {

        int[] num = {100, 5, 10, 55, 60, 7, 17, 91, 33, 82};
        int n = num.length;
        mergeSort(num, 0, n - 1);

        for (int i = 0; i < n; i++)
            System.out.print(num[i]+ " ");
    }

    public static void mergeSort(int[] elements, int low, int high) {
        if (low < high) {
            int mid = (low + high) / 2;
            mergeSort(elements, low, mid);
            mergeSort(elements, mid + 1, high);
            merge(elements, low, mid, high);
        }
    }
    private static void merge(int[] subset, int low, int mid, int high) {

        int n = high-low+1;
        int[] Temp = new int[n];

        int i = low, j = mid + 1;
        int k = 0;

        while (i <= mid || j <= high) {
            if (i > mid)
                Temp[k++] = subset[j++];
            else if (j > high)
                Temp[k++] = subset[i++];
            else if (subset[i] < subset[j])
                Temp[k++] = subset[i++];
            else
                Temp[k++] = subset[j++];
        }
        for (j = 0; j < n; j++)
            subset[low + j] = Temp[j];
    }*/
/*----------------------------------------- end Merge sort --------------------*/

/*----------------------------------------- Heap sort ------------------------*/
public static void main(String[] args) {

    int[] num = {100, 5, 10, 55, 60, 7, 17, 91, 33, 82};
    heapsort(num);
    for (int i = 0; i < num.length; i++)
        System.out.print(num[i]+ " ");
}
    public static void heapsort(int[] a) {
        for (int i = a.length / 2 - 1; i >= 0; i--)
            shiftDown(a, i, a.length);
        for (int i = a.length - 1; i > 0; i--) {
            swap(a, 0, i);
            shiftDown(a, 0, i);
        }
    }
    private static void shiftDown(int[] a, int i, int n) {
        int child;
        int tmp;

        for (tmp = a[i]; leftChild(i) < n; i = child) {
            child = leftChild(i);
            if (child != n - 1 && (a[child] < a[child + 1]))
                child++;
            if (tmp < a[child])
                a[i] = a[child];
            else
                break;
        }
        a[i] = tmp;
    }

    private static int leftChild(int i) {
        return 2 * i + 1;
    }

    public static void swap(int[] numbers, int i, int j) {
        int temp = numbers[i];
        numbers[i] = numbers[j];
        numbers[j] = temp;
    }
/*----------------------------------------- end Heap sort --------------------*/

}
