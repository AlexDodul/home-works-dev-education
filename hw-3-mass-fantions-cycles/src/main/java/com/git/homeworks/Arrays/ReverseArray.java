package com.git.homeworks.Arrays;

public class ReverseArray {
    public static void main(String[] args) {
        int [] arr = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int [] arrReverse = new int[arr.length];
        int count = 0;

        for (int i = arr.length - 1; i >= 0 ; i--) {
            arrReverse [count] = arr[i];
            count++;
        }
        for (int j = 0; j < arrReverse.length; j++) {
            System.out.print(arrReverse[j] + " ");
        }
    }
}
