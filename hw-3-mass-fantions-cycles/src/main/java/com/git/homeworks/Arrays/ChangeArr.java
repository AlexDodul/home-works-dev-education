package com.git.homeworks.Arrays;

public class ChangeArr {
    public static void main(String[] args) {
        int[] arr = new int[]{1, 2, 3, 4, 5, 6, 7, 8};
        int size = arr.length / 2;

        for (int i = 0; i < size; i++) {
            int a = arr[size + i];
            arr[size + i] = arr[i];
            arr[i] = a;
        }

        for (int j = 0; j < arr.length; j++) {
            System.out.print(arr[j] + " ");
        }
    }
}
