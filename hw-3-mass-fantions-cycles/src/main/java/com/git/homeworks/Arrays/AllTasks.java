package com.git.homeworks.Arrays;

import java.util.Arrays;

public class AllTasks {
    public static void main(String[] args) {
        System.out.println(minimumElement(new int[]{5, 10, 8, 0, -2, 7, 3}));
        System.out.println(maximumElement(new int[]{5, 10, 8, 0, -2, 7, 3}));
        System.out.println(minimumIndex(new int[]{5, 10, 8, 0, -2, 7, 3}));
        System.out.println(maximumIndex(new int[]{5, 10, 8, 0, -2, 7, 3}));
        System.out.println(sumOfOddElements(new int[]{5, 10, 8, 0, -2, 7, 3}));
        System.out.println(Arrays.toString(reverseArray(new int[]{1, 2, 3, 4, 5, 6, 7})));
        System.out.println(numberOfOddElements(new int[]{1, 2, 3, 4, 5, 6, 7}));
        System.out.println(Arrays.toString(changeArr(new int[]{1, 2, 3, 4, 5, 6, 7, 8})));
        System.out.println(Arrays.toString(bubbleSort(new int[]{5, 10, 8, 0, -2, 7, 3})));
        System.out.println(Arrays.toString(insertSort(new int[]{5, 10, 8, 0, -2, 7, 3})));
        System.out.println(Arrays.toString(selectSort(new int[]{5, 10, 8, 0, -2, 7, 3})));
        System.out.println(Arrays.toString(shellSort(new int[]{5, 10, 8, 0, -2, 7, 3})));
    }

    //==============Task 1=================
    public static int minimumElement(int[] arr) {
        int min = arr[0];
        for (int i = 0; i < arr.length; i++) {
            if (min > arr[i]) {
                min = arr[i];
            }
        }
        return min;
    }

    //==============Task 2=================
    public static int maximumElement(int[] arr) {
        int max = arr[0];
        for (int i = 0; i < arr.length; i++) {
            if (max < arr[i]) {
                max = arr[i];
            }
        }
        return max;
    }

    //==============Task 3=================
    public static int minimumIndex(int[] arr) {
        int min = arr[0];
        int index = 0;
        for (int i = 0; i < arr.length; i++) {
            if (min > arr[i]) {
                min = arr[i];
                index = i;
            }
        }
        return index;
    }

    //==============Task 4=================
    public static int maximumIndex(int[] arr) {
        int max = arr[0];
        int index = 0;

        for (int i = 0; i < arr.length; i++) {
            if (max < arr[i]) {
                max = arr[i];
                index = i;
            }
        }
        return index;
    }

    //==============Task 5=================
    public static int sumOfOddElements(int[] arr) {
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            if (i % 2 != 0) {
                sum += arr[i];
            }
        }
        return sum;
    }

    //==============Task 6=================
    public static int[] reverseArray(int[] arr) {
        int[] arrReverse = new int[arr.length];
        int count = 0;

        for (int i = arr.length - 1; i >= 0; i--) {
            arrReverse[count] = arr[i];
            count++;
        }
        return arrReverse;
    }

    //==============Task 7=================
    public static int numberOfOddElements(int[] arr) {
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 != 0) {
                count++;
            }
        }
        return count;
    }

    //==============Task 8=================
    public static int[] changeArr(int[] arr) {
        int size = arr.length / 2;

        for (int i = 0; i < size; i++) {
            int a = arr[size + i];
            arr[size + i] = arr[i];
            arr[i] = a;
        }
        return arr;
    }

    //==============Task 9=================
    //-------------------------- Bubble Sort -----------------------
    public static int[] bubbleSort(int[] arr) {
        boolean sorted = false;
        int temp;
        while (!sorted) {
            sorted = true;
            for (int i = 0; i < arr.length - 1; i++) {
                if (arr[i] > arr[i + 1]) {
                    temp = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = temp;
                    sorted = false;
                }
            }
        }
        return arr;
    }

    //-------------------------- Insert Sort -----------------------
    public static int[] insertSort(int[] array) {
        for (int i = 1; i < array.length; i++) {
            int current = array[i];
            int j = i - 1;
            while (j >= 0 && current < array[j]) {
                array[j + 1] = array[j];
                j--;
            }
            array[j + 1] = current;
        }
        return array;
    }

    //-------------------------- Select Sort -----------------------
    public static int[] selectSort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int min = array[i];
            int minId = i;
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < min) {
                    min = array[j];
                    minId = j;
                }
            }
            int temp = array[i];
            array[i] = min;
            array[minId] = temp;
        }
        return array;
    }

    //==============Task 10=================
    //-------------------------- Shell Sort -----------------------
    public static int[] shellSort(int[] numbers) {
        int j;
        for (int gap = numbers.length / 2; gap > 0; gap /= 2) {
            for (int i = gap; i < numbers.length; i++) {
                int temp = numbers[i];
                for (j = i; j >= gap && numbers[j - gap] > temp; j -= gap) {
                    numbers[j] = numbers[j - gap];
                }
                numbers[j] = temp;
            }
        }
        return numbers;
    }
}
