package org.bitbucket.hwVertionNoSerialization;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.*;
import javax.imageio.ImageIO;
import javax.swing.JComponent;


public class Canvas extends JComponent {
    private int X1, Y1, X2, Y2;
    private Graphics2D g;
    private Image img;
    private Image background;
    private Rectangle shape;

    public void save(File file) {
        try {
            ImageIO.write((RenderedImage) img, "PNG", file);
        } catch (IOException ex) {
        }
    }

    public void load(File file) {
        try {
            img = ImageIO.read(file);
            g = (Graphics2D) img.getGraphics();
        } catch (IOException ex) {
        }
    }

    protected void paintComponent(Graphics g1) {
        if (img == null) {
            img = createImage(getSize().width, getSize().height);
            g = (Graphics2D) img.getGraphics();
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                    RenderingHints.VALUE_ANTIALIAS_ON);
            clear();
        }
        g1.drawImage(img, 0, 0, null);

        if (shape != null) {
            Graphics2D g2d = g;
            g2d.draw(shape);
        }
    }

    public Canvas() {
        setBackground(Color.WHITE);
        defaultListener();
    }

    public void defaultListener() {
        setDoubleBuffered(false);
        MouseListener listener = new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                X2 = e.getX();
                Y2 = e.getY();
            }
        };

        MouseMotionListener motion = new MouseMotionAdapter() {
            public void mouseDragged(MouseEvent e) {
                X1 = e.getX();
                Y1 = e.getY();

                if (g != null) {
                    g.drawLine(X2, Y2, X1, Y1);
                    repaint();
                    X2 = X1;
                    Y2 = Y1;
                }
            }
        };
        addMouseListener(listener);
        addMouseMotionListener(motion);
    }

    public void picker(Color color) {
        g.setPaint(color);
    }

    public void clear() {
        if (background != null) {
            setImage(copyImage(background));
        } else {
            g.setPaint(Color.white);
            g.fillRect(0, 0, getSize().width, getSize().height);
            g.setPaint(Color.black);
        }
        repaint();
    }

    private void setImage(Image img) {
        g = (Graphics2D) img.getGraphics();
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        g.setPaint(Color.black);
        this.img = img;
        repaint();
    }

    private BufferedImage copyImage(Image img) {
        BufferedImage copyOfImage = new BufferedImage(getSize().width,
                getSize().height, BufferedImage.TYPE_INT_RGB);
        Graphics g = copyOfImage.createGraphics();
        g.drawImage(img, 0, 0, getWidth(), getHeight(), null);
        return copyOfImage;
    }

    public void setThickness(int thick) {
        g.setStroke(new BasicStroke(thick));
    }
}


