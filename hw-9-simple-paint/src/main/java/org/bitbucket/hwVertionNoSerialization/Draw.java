package org.bitbucket.hwVertionNoSerialization;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class Draw {

    Canvas canvas;
    Color color = Color.WHITE;
    JButton clearButton, colorPicker, loadButton,
            saveAsButton;
    private JFileChooser fileChooser;
    private File file;
    private int saveCounter = 0;
    private JLabel filenameBar, thicknessStat;
    private JSlider thicknessSlider;
    private int width, height;
    ChangeListener thick = new ChangeListener() {
        public void stateChanged(ChangeEvent e) {
            thicknessStat.setText(String.format("%s",
                    thicknessSlider.getValue()));
            canvas.setThickness(thicknessSlider.getValue());
        }
    };
    ActionListener listener = new ActionListener() {

        public void actionPerformed(ActionEvent event) {
            if (event.getSource() == clearButton) {
                canvas.clear();
            } else if (event.getSource() == saveAsButton) {
                saveCounter = 1;
                fileChooser = new JFileChooser();
                if (fileChooser.showSaveDialog(saveAsButton) == JFileChooser.APPROVE_OPTION) {
                    file = fileChooser.getSelectedFile();
                    filenameBar.setText(file.toString());
                    canvas.save(file);
                }
            } else if (event.getSource() == loadButton) {
                fileChooser = new JFileChooser();
                if (fileChooser.showOpenDialog(loadButton) == JFileChooser.APPROVE_OPTION) {
                    file = fileChooser.getSelectedFile();
                    filenameBar.setText(file.toString());
                    canvas.load(file);
                }
            } else if (event.getSource() == colorPicker) {
                color = JColorChooser.showDialog(null, "Pick your color!",
                        color);
                if (color == null)
                    color = (Color.WHITE);
                canvas.picker(color);
            }
        }
    };

    public void setWH(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public void openPaint() {
        LookAndFeelInfo[] lookAndFeelInfo = UIManager.getInstalledLookAndFeels();
        for (LookAndFeelInfo info : lookAndFeelInfo) {
            if (info.getName().contains("Nimbus")) {
                try {
                    UIManager.setLookAndFeel(info.getClassName());
                } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
                    e.printStackTrace();
                }
            }
        }
        JFrame frame = new JFrame("Simple Paint");
        Container container = frame.getContentPane();
        container.setLayout(new BorderLayout());
        canvas = new Canvas();
        container.add(canvas, BorderLayout.CENTER);
        JPanel panel = new JPanel();
        JPanel panel1 = new JPanel();
        Box box = Box.createVerticalBox();
        panel1.setLayout(new FlowLayout());

        thicknessSlider = new JSlider(JSlider.HORIZONTAL, 0, 50, 1);
        thicknessSlider.setMajorTickSpacing(25);
        thicknessSlider.setPaintTicks(true);
        thicknessSlider.setPreferredSize(new Dimension(50, 40));
        thicknessSlider.addChangeListener(thick);
        saveAsButton = new JButton("Save As");
        saveAsButton.addActionListener(listener);
        loadButton = new JButton("Load");
        loadButton.addActionListener(listener);
        colorPicker = new JButton("Color Picker");
        colorPicker.addActionListener(listener);
        clearButton = new JButton("Clear");
        clearButton.addActionListener(listener);
        filenameBar = new JLabel("No file");
        thicknessStat = new JLabel("1");

        panel1.add(filenameBar);
        panel.add(thicknessStat);
        panel.add(thicknessSlider);
        panel.add(colorPicker);
        panel.add(saveAsButton);
        panel.add(loadButton);
        panel.add(clearButton);

        container.add(panel, BorderLayout.NORTH);
        container.add(panel1, BorderLayout.SOUTH);
        container.add(box, BorderLayout.WEST);
        frame.setVisible(true);
        frame.setSize(width, height);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }
}
