package org.bitbucket.simplePaint.config;

import org.bitbucket.simplePaint.formats.IBaseFormat;

public class Fm {
    public static IBaseFormat newInstance(String description) {
        IBaseFormat result;
        switch (description) {
            case "json":
                result = FormatChoose.jsonFormat();
                break;
            case "csv":
                result = FormatChoose.csvFormat();
                break;
            case "bin":
                result = FormatChoose.binFormat();
                break;
            case "xml":
                result = FormatChoose.xmlFormat();
                break;
            case "yml":
                result = FormatChoose.ymlFormat();
                break;
            default:
                throw new IllegalArgumentException("File format is not valid.\n" +
                        "Please choose one format: 'json', 'csv', 'bin', 'xml' 'yaml'.");
        }
        return result;
    }
}
