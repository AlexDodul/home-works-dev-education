package org.bitbucket.simplePaint.config;

import org.bitbucket.simplePaint.formats.impl.*;
import org.bitbucket.simplePaint.formats.IBaseFormat;

public class FormatChoose {
    public static IBaseFormat csvFormat(){
        return new CsvFormat();
    }

    public static IBaseFormat binFormat(){
        return new BinFormat();
    }

    public static IBaseFormat jsonFormat(){
        return new JsonFormat();
    }

    public static IBaseFormat xmlFormat(){
        return new XmlFormat();
    }

    public static IBaseFormat ymlFormat() {
        return new YamlFormat();
    }
}
