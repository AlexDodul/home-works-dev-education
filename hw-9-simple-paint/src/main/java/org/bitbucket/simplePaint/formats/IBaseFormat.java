package org.bitbucket.simplePaint.formats;

import org.bitbucket.simplePaint.CustomLine;

import java.util.List;

public interface IBaseFormat {

    void toFormat (List <CustomLine> lines, String file);

    List<CustomLine> fromFormat (String file);
}
