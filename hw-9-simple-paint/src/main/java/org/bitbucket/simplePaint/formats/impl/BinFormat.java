package org.bitbucket.simplePaint.formats.impl;

import org.bitbucket.simplePaint.CustomLine;
import org.bitbucket.simplePaint.formats.IBaseFormat;

import javax.swing.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static com.sun.jmx.mbeanserver.Util.cast;

public class BinFormat implements IBaseFormat {

    @Override
    public void toFormat(List<CustomLine> lines, String file) {
        try (ObjectOutputStream o = new ObjectOutputStream(new FileOutputStream(file))) {
            o.writeObject(lines);
            JOptionPane.showMessageDialog(null, "File saved","File Saved",JOptionPane.INFORMATION_MESSAGE);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public List<CustomLine> fromFormat(String file) {
        try (ObjectInputStream o = new ObjectInputStream(new FileInputStream(file))) {
            return cast(o.readObject());
        } catch (ClassNotFoundException | IOException e) {
            return new ArrayList<>();
        }
    }
}
