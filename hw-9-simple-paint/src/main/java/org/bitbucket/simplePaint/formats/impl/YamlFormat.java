package org.bitbucket.simplePaint.formats.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.google.gson.reflect.TypeToken;
import org.bitbucket.simplePaint.CustomLine;
import org.bitbucket.simplePaint.formats.IBaseFormat;

import javax.swing.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class YamlFormat implements IBaseFormat {
    @Override
    public void toFormat(List<CustomLine> lines, String file) {
        try {
            ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
            String ymlString = mapper.writeValueAsString(lines);
            try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
                bw.write(ymlString);
                JOptionPane.showMessageDialog(null, "File saved","File Saved",JOptionPane.INFORMATION_MESSAGE);
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<CustomLine> fromFormat(String file) {
        List <CustomLine> lines = new ArrayList<>();
        try {
            Type type = new TypeToken<List<CustomLine>>() {}.getType();
            ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
            lines = mapper.readValue(new File(file), mapper.constructType(type));
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        return lines;
    }
}