package org.bitbucket.simplePaint;

import javax.swing.*;
import java.awt.*;

public class PaintFrame extends JFrame {
    public PaintFrame(PaintPanel paintPanel, PaintFilePanel paintFilePanel,
                      PaintChangeLinePanel paintChangeLinePanel) throws HeadlessException {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(null);
        setBounds(10, 10, 1150, 820);
        add(paintPanel);
        add(paintFilePanel);
        add(paintChangeLinePanel);
        setResizable(false);
        setVisible(Boolean.TRUE);
    }
}
