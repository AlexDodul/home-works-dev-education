package org.bitbucket.simplePaint;

import org.bitbucket.simplePaint.commands.PaintFileCommands;

import javax.swing.*;

public class PaintFilePanel extends JPanel {


    public static JTextField file;

    public static JLabel lable;

    public PaintFilePanel(PaintFileCommands paintFileCommands) {
        UIManager.LookAndFeelInfo[] lookAndFeelInfo = UIManager.getInstalledLookAndFeels();
        for (UIManager.LookAndFeelInfo info : lookAndFeelInfo) {
            if (info.getName().contains("Nimbus")) {
                try {
                    UIManager.setLookAndFeel(info.getClassName());
                } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
                    e.printStackTrace();
                }
            }
        }
        setLayout(null);
        setBounds(920, 10, 200, 200);

        JButton btnSave = new JButton("Save");
        JButton btnOpen = new JButton("Open");
        lable = new JLabel("Open file name:");
        file = new JTextField(paintFileCommands.fileChose);

        btnOpen.addActionListener(paintFileCommands.actionOpen());
        btnSave.addActionListener(paintFileCommands.actionSave());

        btnSave.setBounds(10, 10, 180, 30);
        btnOpen.setBounds(10, 70, 180, 30);

        add(btnSave);
        add(btnOpen);

        setVisible(Boolean.TRUE);
    }
}
