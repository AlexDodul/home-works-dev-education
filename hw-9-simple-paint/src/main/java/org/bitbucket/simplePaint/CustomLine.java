package org.bitbucket.simplePaint;

import java.awt.*;
import java.util.Objects;

public class CustomLine {

    private int firstX, firstY, lastX, lastY;

    //private Color color;

    private int color;

    private int width;

    public CustomLine() {

    }

    public CustomLine(int firstX, int firstY, int lastX, int lastY, int color, int width) {
        this.firstX = firstX;
        this.firstY = firstY;
        this.lastX = lastX;
        this.lastY = lastY;
        this.color = color;
        this.width = width;
    }

    public int getFirstX() {
        return firstX;
    }

    public int getFirstY() {
        return firstY;
    }

    public int getLastX() {
        return lastX;
    }

    public int getLastY() {
        return lastY;
    }

    public int getColor() {
        return color;
    }

    public int getWidth() {
        return width;
    }

    public void setFirstX(int firstX) {
        this.firstX = firstX;
    }

    public void setFirstY(int firstY) {
        this.firstY = firstY;
    }

    public void setLastX(int lastX) {
        this.lastX = lastX;
    }

    public void setLastY(int lastY) {
        this.lastY = lastY;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CustomLine that = (CustomLine) o;
        return firstX == that.firstX &&
                firstY == that.firstY &&
                lastX == that.lastX &&
                lastY == that.lastY &&
                color == that.color &&
                width == that.width;
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstX, firstY, lastX, lastY, color, width);
    }

    @Override
    public String toString() {
        return "CustomLine{" +
                "firstX=" + firstX +
                ", firstY=" + firstY +
                ", lastX=" + lastX +
                ", lastY=" + lastY +
                ", color=" + color +
                ", width=" + width +
                '}';
    }
}
