package org.bitbucket.simplePaint;

import org.bitbucket.simplePaint.commands.PaintChangeLineCommand;

import javax.swing.*;

public class PaintChangeLinePanel extends JPanel {

    public static JLabel jLabel;

    public static JSlider btnThickness;

    public PaintChangeLinePanel(PaintChangeLineCommand paintChangeLineCommand) {
        UIManager.LookAndFeelInfo[] lookAndFeelInfo = UIManager.getInstalledLookAndFeels();
        for (UIManager.LookAndFeelInfo info : lookAndFeelInfo) {
            if (info.getName().contains("Nimbus")) {
                try {
                    UIManager.setLookAndFeel(info.getClassName());
                } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
                    e.printStackTrace();
                }
            }
        }

        setLayout(null);
        setBounds(920, 220, 200, 200);

        JButton btnClear = new JButton("Clear");
        JButton btnColor = new JButton("Choose Color");
        jLabel = new JLabel("Thickness");
        btnThickness = new JSlider(JSlider.HORIZONTAL, 0, 50, 1);

        btnColor.setBounds(10, 10, 180, 30);
        btnThickness.setBounds(10, 70, 180, 30);
        btnClear.setBounds(10, 130, 180, 30);

        btnThickness.setMajorTickSpacing(10);
        btnThickness.setPaintTicks(Boolean.TRUE);

        btnClear.addActionListener(paintChangeLineCommand.actionClear());
        btnColor.addActionListener(paintChangeLineCommand.actionColor());
        btnThickness.addChangeListener(paintChangeLineCommand.actionThickness());

        add(btnColor);
        add(btnThickness);
        add(btnClear);

        setVisible(Boolean.TRUE);
    }
}
