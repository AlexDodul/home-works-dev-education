package org.bitbucket.simplePaint.commands;

import org.bitbucket.simplePaint.PaintChangeLinePanel;
import org.bitbucket.simplePaint.PaintPanel;

import javax.swing.*;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionListener;

public class PaintChangeLineCommand {

    private PaintPanel paintPanel;

    public void setPaintPanel(PaintPanel paintPanel) {
        this.paintPanel = paintPanel;
    }

    public ActionListener actionClear() {
        return e -> {
            paintPanel.getLines().clear();
            paintPanel.repaint();
        };
    }

    public ActionListener actionColor() {
        return e -> {
            paintPanel.setColor(JColorChooser.showDialog(new JFrame(), "Choose a color", Color.BLACK).getRGB());
        };
    }

    public ChangeListener actionThickness() {
        return e -> {
            PaintChangeLinePanel.jLabel.setText(String.format("Thickness \n %s", PaintChangeLinePanel.btnThickness.getValue()));
            paintPanel.setWidth(PaintChangeLinePanel.btnThickness.getValue());
        };
    }
}
