package org.bitbucket.simplePaint;

import org.bitbucket.simplePaint.commands.PaintChangeLineCommand;
import org.bitbucket.simplePaint.commands.PaintFileCommands;

import java.awt.*;

public class ApplicationPaint {
    public static void main(String[] args) {
        PaintFileCommands fileCommands = new PaintFileCommands();
        PaintChangeLineCommand paintChangeLineCommand = new PaintChangeLineCommand();
        PaintPanel paintPanel = new PaintPanel();
        fileCommands.setPaintPanel(paintPanel);
        paintChangeLineCommand.setPaintPanel(paintPanel);
        new PaintFrame(paintPanel, new PaintFilePanel(fileCommands), new PaintChangeLinePanel(paintChangeLineCommand))
                .getContentPane().setBackground(Color.BLACK);
    }
}
