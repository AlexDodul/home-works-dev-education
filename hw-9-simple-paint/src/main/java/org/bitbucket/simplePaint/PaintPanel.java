package org.bitbucket.simplePaint;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.List;

public class PaintPanel extends JPanel implements MouseListener, MouseMotionListener {

    private int x, y;

    private int color;

    private int width;

    private Graphics2D g;

    private Image background;

    private List<CustomLine> lines = new ArrayList<>();

    public PaintPanel() {
        setBounds(10, 10, 900, 760);
        addMouseListener(this);
        addMouseMotionListener(this);
        setVisible(Boolean.TRUE);
    }

    public List<CustomLine> getLines() {
        return lines;
    }

    public void setLines(List<CustomLine> lines) {
        this.lines = lines;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public void setWidth(int width){
        this.width = width;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2d = (Graphics2D) g;
        for (CustomLine line : this.lines) {
            g2d.setStroke(new BasicStroke(line.getWidth()));
            g2d.setColor(new Color(line.getColor()));
            g2d.drawLine(line.getFirstX(), line.getFirstY(), line.getLastX(), line.getLastY());
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {
        this.x = e.getX();
        this.y = e.getY();
    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseDragged(MouseEvent e) {
        int newX = e.getX();
        int newY = e.getY();
        this.lines.add(new CustomLine(this.x, this.y, newX, newY, this.color, this.width));
        this.x = newX;
        this.y = newY;
        repaint();
    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }

}
