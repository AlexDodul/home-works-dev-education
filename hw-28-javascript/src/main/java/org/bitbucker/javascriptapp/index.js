const data = [
    {
        "login": "Jeannie_23",
        "name": "Jeannie Randolph",
        "pass": "$3z&Se8Ph8",
    },
    {
        "login": "k_brown",
        "name": "Kathie Brown",
        "pass": "*7Gf5iNy7@",
    },
    {
        "login": "mega_powers",
        "name": "Powers Justice",
        "pass": "8*DkE87jb&",
    },
    {
        "login": "o.neal",
        "name": "Morales Oneal",
        "pass": "68Zp6-@vUp",
    },
    {
        "login": "A-dela",
        "name": "Adela Heath",
        "pass": "83-2pgJSj$",
    }
]

const root = document.getElementById('root');

const form = document.createElement('form');

const div1 = document.createElement('div');
div1.id = 'div1';
div1.className = 'fields';

const div2 = document.createElement('div');
div2.className = 'fields';

const div3 = document.createElement('div');
div3.className = 'fields';

const fName = document.createElement('input');
fName.className = 'fName';
fName.setAttribute('placeholder', 'First Name');
fName.style.outline = 'none';

const lName = document.createElement('input');
lName.className = 'lName';
lName.setAttribute('placeholder', 'Last Name');
lName.style.outline = 'none';

const password = document.createElement('input');
password.className = 'pass'
password.setAttribute('placeholder', 'Password');
password.setAttribute('type', 'password');

const btn = document.createElement('button');
btn.className = 'button';
btn.setAttribute('type', 'button');

const textnode = document.createTextNode('Авторизоваться');
btn.appendChild(textnode);

document.getElementById('div1')

div1.appendChild(fName);
div2.appendChild(lName);
div3.appendChild(password);
form.appendChild(div1);
form.appendChild(div2);
form.appendChild(div3);
form.appendChild(btn);
root.appendChild(form);

btn.addEventListener('click', (e) => {
    sendData();
})

function sendData(){
    const allName = fName.value + ' ' + lName.value;
    if (!fName.value || !lName.value || !password.value) {
        if (!fName.value) {
            fName.style.borderColor = 'red';
            alert('Ошибка валидации: Введите Имя!');
        }
        if (!lName.value) {
            lName.style.borderColor = 'red';
            alert('Ошибка валидации: Введите Фамилию!');
        }
        if (!password.value) {
            password.style.borderColor = 'red';
            alert('Ошибка валидации: Введите Пароль!');
        }
    } else {
        auth(allName, password.value);
    }
}

fName.addEventListener('input', (e) => {
    const regex = /^[A-Za-z-]/;
    let checkFName = regex.test(fName.value);
    if (e.target.value.length < 2) {
        fName.style.borderColor = 'orange';
    } else if (e.target.value.length > 10) {
        fName.value = e.target.value.substr(0, 10);
    } else {
        fName.style.borderColor = 'black';
    }
    if (checkFName != true) {
        fName.style.borderColor = 'red';
        fName.style.boxShadow = '0 0 2px red';
    }
})

lName.addEventListener('input', (e) => {
    const regex = /^[A-Za-z-]/;
    let checkLName = regex.test(lName.value);
    if (e.target.value.length < 2) {
        lName.style.borderColor = 'orange';
    } else if (e.target.value.length > 20) {
        lName.value = e.target.value.substr(0, 20);
    } else {
        lName.style.borderColor = 'black';
    }
    if (checkLName != true) {
        lName.style.borderColor = 'red';
        lName.style.boxShadow = '0 0 2px red';
    }
})

password.addEventListener('input', (e) => {
    const regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$&@*-])[A-Za-z\d$&@*-]/;
    let checkPass = regex.test(password.value);
    if (e.target.value.length < 5) {
        password.style.borderColor = 'orange';
    } else if (e.target.value.length > 10) {
        password.value = e.target.value.substr(0, 10);
    } else {
        password.style.borderColor = 'black';
    }
    if (checkPass !== true) {
        password.style.borderColor = 'red';
        password.style.boxShadow = '0 0 2px red';
    }
})

function auth(name, password) {
    let flag = false;
    let i = 0;
    let userName = '';
    while (i < data.length){
        if (data[i].name === name && data[i].pass === password){
            userName = data[i].login;
            flag = true;
            i = data.length + 1;
        }
        i++;
    }
    if (flag === true){
        alert('Вы авторизовались как ' + userName + ' - ' + name)
    } else {
        alert('Ошибка авторизации!')
    }
}

window.addEventListener('keypress', (e) => {
    if (e.key === 'Enter'){
        sendData();
    }
})
