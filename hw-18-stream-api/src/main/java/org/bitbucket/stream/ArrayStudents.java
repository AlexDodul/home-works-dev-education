package org.bitbucket.stream;

import java.util.ArrayList;
import java.util.List;

public class ArrayStudents {

    public static List<Student> studentList = new ArrayList<>();

    public static void init() {
        studentList.clear();
        studentList.add(new Student(1, "Alex", "Dodul", 1987, "Odessa", "050123456789", "2 faculty", "3", "530"));
        studentList.add(new Student(2, "Sasha", "Bars", 1991, "Kharkov", "0500000001", "4 faculty", "1", "440"));
        studentList.add(new Student(3, "Dima", "Aks", 1992, "Kiev", "0500000002", "5 faculty", "1", "230"));
        studentList.add(new Student(4, "Sergey", "Kim", 1990, "Odessa", "0500000003", "3 faculty", "2", "310"));
        studentList.add(new Student(5, "Alex", "Kovin", 1999, "Dnepr", "0500000004", "2 faculty", "3", "210"));
        studentList.add(new Student(6, "Nikolay", "Kolya", 1996, "Odessa", "0500000005", "5 faculty", "4", "530"));
    }
}
