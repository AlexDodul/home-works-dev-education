package org.bitbucket.stream;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.bitbucket.stream.MethodsStudents.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class MethodsStudentsTest {

    @Before
    public void setUp(){
        ArrayStudents.init();
    }

    /*============================================================================*/
    /*=========================== getFaculty =====================================*/
    /*============================================================================*/

    @Test
    public void getFacultyMany (){
        List<Student> exp = new ArrayList<>();
        exp.add(new Student(3, "Dima", "Aks", 1992, "Kiev", "0500000002", "5 faculty", "1", "230"));
        exp.add(new Student(6, "Nikolay", "Kolya", 1996, "Odessa", "0500000005", "5 faculty", "4", "530"));
        List<Student> act = getFaculty("5 faculty");
        assertEquals(exp, act);
    }

    @Test
    public void getFacultyOne (){
        List<Student> exp = new ArrayList<>();
        exp.add(new Student(4, "Sergey", "Kim", 1990, "Odessa", "0500000003", "3 faculty", "2", "310"));
        List<Student> act = getFaculty("3 faculty");
        assertEquals(exp, act);
    }

    @Test
    public void getFacultyNull() {
        List<Student> exp = new ArrayList<>();
        List<Student> act = getFaculty(null);
        assertEquals(act, exp);
    }

    @Test
    public void getFacultyEmpty() {
        List<Student> exp = new ArrayList<>();
        List<Student> act = getFaculty("");
        assertEquals(act, exp);
    }

    /*============================================================================*/
    /*======================== facultyAndCourse ==================================*/
    /*============================================================================*/

    @Test
    public void facultyAndCourseMany (){
        List<Student> exp = new ArrayList<>();
        exp.add(new Student(1, "Alex", "Dodul", 1987, "Odessa", "050123456789", "2 faculty", "3", "530"));
        exp.add(new Student(5, "Alex", "Kovin", 1999, "Dnepr", "0500000004", "2 faculty", "3", "210"));
        List<Student> act = facultyAndCourse("2 faculty", "3");
        assertEquals(exp, act);
    }

    @Test
    public void facultyAndCourseOne (){
        List<Student> exp = new ArrayList<>();
        exp.add(new Student(6, "Nikolay", "Kolya", 1996, "Odessa", "0500000005", "5 faculty", "4", "530"));
        List<Student> act = facultyAndCourse("5 faculty", "4");
        assertEquals(exp, act);
    }

    @Test
    public void facultyAndCourseNull() {
        List<Student> act = facultyAndCourse(null, "1");
        assertNull(act);
    }

    @Test
    public void facultyAndCourseEmpty() {
        List<Student> act = facultyAndCourse("", "2");
        assertNull(act);
    }

    @Test
    public void facultyAndCourseNullTwo() {
        List<Student> act = facultyAndCourse("5 faculty", null);
        assertNull(act);
    }

    @Test
    public void facultyAndCourseEmptyTwo() {
        List<Student> act = facultyAndCourse("5 faculty", "");
        assertNull(act);
    }

    /*============================================================================*/
    /*============================ afterYear =====================================*/
    /*============================================================================*/

    @Test
    public void afterYearMany() {
        List<Student> exp = new ArrayList<>();
        exp.add(new Student(2, "Sasha", "Bars", 1991, "Kharkov", "0500000001", "4 faculty", "1", "440"));
        exp.add(new Student(3, "Dima", "Aks", 1992, "Kiev", "0500000002", "5 faculty", "1", "230"));
        exp.add(new Student(4, "Sergey", "Kim", 1990, "Odessa", "0500000003", "3 faculty", "2", "310"));
        exp.add(new Student(5, "Alex", "Kovin", 1999, "Dnepr", "0500000004", "2 faculty", "3", "210"));
        exp.add(new Student(6, "Nikolay", "Kolya", 1996, "Odessa", "0500000005", "5 faculty", "4", "530"));
        List<Student> act = afterYear(1987);
        assertEquals(exp, act);
    }

    @Test
    public void afterYearOne() {
        List<Student> exp = new ArrayList<>();
        exp.add(new Student(5, "Alex", "Kovin", 1999, "Dnepr", "0500000004", "2 faculty", "3", "210"));
        List<Student> act = afterYear(1998);
        assertEquals(exp, act);
    }

    @Test
    public void afterYearMore() {
        List<Student> exp = new ArrayList<>();
        List<Student> act = afterYear(2000);
        assertEquals(exp, act);
    }


    @Test
    public void afterYearOdd() {
        List<Student> act = afterYear(-2);
        assertNull(act);
    }

    @Test
    public void afterYearZero() {
        List<Student> act = afterYear(0);
        assertNull(act);
    }

    /*============================================================================*/
    /*============================ firstYear =====================================*/
    /*============================================================================*/

    @Test
    public void firstYearOne() {
        List<Student> exp = new ArrayList<>();
        exp.add(new Student(2, "Sasha", "Bars", 1991, "Kharkov", "0500000001", "4 faculty", "1", "440"));
        List<Student> act = firstYear(1987);
        assertEquals(exp, act);
    }

    @Test
    public void firstYearTwo() {
        List<Student> exp = new ArrayList<>();
        exp.add(new Student(3, "Dima", "Aks", 1992, "Kiev", "0500000002", "5 faculty", "1", "230"));
        List<Student> act = firstYear(1991);
        assertEquals(exp, act);
    }

    @Test
    public void firstYearOdd() {
        List<Student> act = firstYear(-2);
        assertNull(act);
    }

    @Test
    public void firstYearZero() {
        List<Student> act = firstYear(0);
        assertNull(act);
    }

    /*============================================================================*/
    /*============================ listGroup =====================================*/
    /*============================================================================*/

    @Test
    public void listGroupMany() {
        List<String> exp = new ArrayList<>();
        exp.add("Alex Dodul");
        exp.add("Nikolay Kolya");
        List<String> act = listGroup("530");
        assertEquals(exp, act);
    }

    @Test
    public void listGroupOne() {
        List<String> exp = new ArrayList<>();
        exp.add("Sergey Kim");
        List<String> act = listGroup("310");
        assertEquals(exp, act);
    }

    @Test
    public void listGroupNull() {
        List<String> act = listGroup(null);
        assertNull(act);
    }

    @Test
    public void listGroupEmpty() {
        List<String> act = listGroup("");
        assertNull(act);
    }

    /*============================================================================*/
    /*=========================== studentCount ===================================*/
    /*============================================================================*/

    @Test
    public void studentCountMany() {
        long exp = 2;
        long act = studentCount("2 faculty");
        assertEquals(act, exp);
    }

    @Test
    public void studentCountOne() {
        long exp = 1;
        long act = studentCount("4 faculty");
        assertEquals(act, exp);
    }


    @Test
    public void studentCountZero() {
        long exp = 0;
        long act = studentCount(null);
        assertEquals(act, exp);
    }

    @Test
    public void studentCountEmpty() {
        long exp = 0;
        long act = studentCount("");
        assertEquals(act, exp);
    }

    /*============================================================================*/
    /*=========================== setStudentsFaculty =============================*/
    /*============================================================================*/

    @Test
    public void setStudentsFacultyMany() {
        List<Student> exp = new ArrayList<>();
        exp.add(new Student(1, "Alex", "Dodul", 1987, "Odessa", "050123456789", "2 faculty", "3", "530"));
        exp.add(new Student(2, "Sasha", "Bars", 1991, "Kharkov", "0500000001", "2 faculty", "1", "440"));
        exp.add(new Student(3, "Dima", "Aks", 1992, "Kiev", "0500000002", "2 faculty", "1", "230"));
        exp.add(new Student(4, "Sergey", "Kim", 1990, "Odessa", "0500000003", "2 faculty", "2", "310"));
        exp.add(new Student(5, "Alex", "Kovin", 1999, "Dnepr", "0500000004", "2 faculty", "3", "210"));
        exp.add(new Student(6, "Nikolay", "Kolya", 1996, "Odessa", "0500000005", "2 faculty", "4", "530"));
        List<Student> act = setStudentsFaculty("2 faculty");
        assertEquals(exp, act);
    }

    @Test
    public void setStudentsFacultyNull() {
        List<Student> act = setStudentsFaculty(null);
        assertNull(act);
    }

    @Test
    public void setStudentsFacultyEmpty() {
        List<Student> act = setStudentsFaculty("");
        assertNull(act);
    }

    /*============================================================================*/
    /*============================= setStudentsGroup =============================*/
    /*============================================================================*/

    @Test
    public void setStudentsGroupMany() {
        List<Student> exp = new ArrayList<>();
        exp.add(new Student(1, "Alex", "Dodul", 1987, "Odessa", "050123456789", "2 faculty", "3", "210"));
        exp.add(new Student(2, "Sasha", "Bars", 1991, "Kharkov", "0500000001", "4 faculty", "1", "210"));
        exp.add(new Student(3, "Dima", "Aks", 1992, "Kiev", "0500000002", "5 faculty", "1", "210"));
        exp.add(new Student(4, "Sergey", "Kim", 1990, "Odessa", "0500000003", "3 faculty", "2", "210"));
        exp.add(new Student(5, "Alex", "Kovin", 1999, "Dnepr", "0500000004", "2 faculty", "3", "210"));
        exp.add(new Student(6, "Nikolay", "Kolya", 1996, "Odessa", "0500000005", "5 faculty", "4", "210"));
        List<Student> act = setStudentsGroup("210");
        assertEquals(exp, act);
    }

    @Test
    public void setStudentsGroupNull() {
        List<Student> act = setStudentsGroup(null);
        assertNull(act);
    }

    @Test
    public void setStudentsGroupEmpty() {
        List<Student> act = setStudentsGroup("");
        assertNull(act);
    }

    /*============================================================================*/
    /*================================ studentFind ===============================*/
    /*============================================================================*/

    @Test
    public void studentFindMany() {
        long exp = 1;
        long act = studentFind("5 faculty");
        assertEquals(act, exp);
    }

    @Test
    public void studentFindOne() {
        long exp = 1;
        long act = studentFind("4 faculty");
        assertEquals(act, exp);
    }


    @Test
    public void studentFindZero() {
        long exp = 0;
        long act = studentFind(null);
        assertEquals(act, exp);
    }

    @Test
    public void studentFindEmpty() {
        long exp = 0;
        long act = studentFind("");
        assertEquals(act, exp);
    }

    /*============================================================================*/
    /*================================ methodReduce ==============================*/
    /*============================================================================*/

    @Test
    public void methodReduceOne() {
        String exp = "Alex Dodul - 2 faculty 530\n" +
                "Sasha Bars - 4 faculty 440\n" +
                "Dima Aks - 5 faculty 230\n" +
                "Sergey Kim - 3 faculty 310\n" +
                "Alex Kovin - 2 faculty 210\n" +
                "Nikolay Kolya - 5 faculty 530\n";
        String act = methodReduce();
        assertEquals(exp, act);
    }

    /*============================================================================*/
    /*================================ groupFaculty ==============================*/
    /*============================================================================*/

    @Test
    public void sortFacultyTest() {
        Map<String, List<Student>> exp = new HashMap<>();
        List<Student> faculty3 = new ArrayList<>();
        faculty3.add(new Student(4, "Sergey", "Kim", 1990, "Odessa", "0500000003", "3 faculty", "2", "310"));

        List<Student> faculty5 = new ArrayList<>();
        faculty5.add(new Student(3, "Dima", "Aks", 1992, "Kiev", "0500000002", "5 faculty", "1", "230"));
        faculty5.add(new Student(6, "Nikolay", "Kolya", 1996, "Odessa", "0500000005", "5 faculty", "4", "530"));

        List<Student> faculty2 = new ArrayList<>();
        faculty2.add(new Student(1, "Alex", "Dodul", 1987, "Odessa", "050123456789", "2 faculty", "3", "530"));
        faculty2.add(new Student(5, "Alex", "Kovin", 1999, "Dnepr", "0500000004", "2 faculty", "3", "210"));

        List<Student> faculty4 = new ArrayList<>();
        faculty4.add(new Student(2, "Sasha", "Bars", 1991, "Kharkov", "0500000001", "4 faculty", "1", "440"));

        exp.put("3 faculty", faculty3);
        exp.put("5 faculty", faculty5);
        exp.put("2 faculty", faculty2);
        exp.put("4 faculty", faculty4);
        Map<String, List<Student>> act = groupFaculty();
        assertEquals(exp, act);
    }

    /*============================================================================*/
    /*================================ groupCourse ===============================*/
    /*============================================================================*/

    @Test
    public void groupCourseTest() {
        Map<String, List<Student>> exp = new HashMap<>();
        List<Student> course1 = new ArrayList<>();
        course1.add(new Student(2, "Sasha", "Bars", 1991, "Kharkov", "0500000001", "4 faculty", "1", "440"));
        course1.add(new Student(3, "Dima", "Aks", 1992, "Kiev", "0500000002", "5 faculty", "1", "230"));

        List<Student> course2 = new ArrayList<>();
        course2.add(new Student(4, "Sergey", "Kim", 1990, "Odessa", "0500000003", "3 faculty", "2", "310"));

        List<Student> course3 = new ArrayList<>();
        course3.add(new Student(1, "Alex", "Dodul", 1987, "Odessa", "050123456789", "2 faculty", "3", "530"));
        course3.add(new Student(5, "Alex", "Kovin", 1999, "Dnepr", "0500000004", "2 faculty", "3", "210"));

        List<Student> course4 = new ArrayList<>();
        course4.add(new Student(6, "Nikolay", "Kolya", 1996, "Odessa", "0500000005", "5 faculty", "4", "530"));

        exp.put("1", course1);
        exp.put("2", course2);
        exp.put("3", course3);
        exp.put("4", course4);
        Map<String, List<Student>> act = groupCourse();
        assertEquals(exp, act);
    }

    /*============================================================================*/
    /*================================= groupGroup ===============================*/
    /*============================================================================*/

    @Test
    public void groupGroupTest() {
        Map<String, List<Student>> exp = new HashMap<>();
        List<Student> groupGroup440 = new ArrayList<>();
        groupGroup440.add(new Student(2, "Sasha", "Bars", 1991, "Kharkov", "0500000001", "4 faculty", "1", "440"));

        List<Student> groupGroup530 = new ArrayList<>();
        groupGroup530.add(new Student(1, "Alex", "Dodul", 1987, "Odessa", "050123456789", "2 faculty", "3", "530"));
        groupGroup530.add(new Student(6, "Nikolay", "Kolya", 1996, "Odessa", "0500000005", "5 faculty", "4", "530"));

        List<Student> groupGroup210 = new ArrayList<>();
        groupGroup210.add(new Student(5, "Alex", "Kovin", 1999, "Dnepr", "0500000004", "2 faculty", "3", "210"));

        List<Student> groupGroup310 = new ArrayList<>();
        groupGroup310.add(new Student(4, "Sergey", "Kim", 1990, "Odessa", "0500000003", "3 faculty", "2", "310"));

        List<Student> groupGroup230 = new ArrayList<>();
        groupGroup230.add(new Student(3, "Dima", "Aks", 1992, "Kiev", "0500000002", "5 faculty", "1", "230"));

        exp.put("440", groupGroup440);
        exp.put("530", groupGroup530);
        exp.put("210", groupGroup210);
        exp.put("310", groupGroup310);
        exp.put("230", groupGroup230);
        Map<String, List<Student>> act = groupGroup();
        assertEquals(exp, act);
    }

    /*============================================================================*/
    /*============================= allStudentsFaculty ===========================*/
    /*============================================================================*/

    @Test
    public void allStudentsFacultyFirst() {
        boolean exp = false;
        boolean act = allStudentsFaculty("2 faculty");
        assertEquals(exp, act);
    }

    @Test
    public void allStudentsFacultyNull() {
        boolean exp = false;
        boolean act = allStudentsFaculty(null);
        assertEquals(act,exp);
    }

    @Test
    public void allStudentsFacultyEmpty() {
        boolean exp = false;
        boolean act = allStudentsFaculty("");
        assertEquals(act, exp);
    }

    /*============================================================================*/
    /*============================= oneStudentFaculty ============================*/
    /*============================================================================*/

    @Test
    public void oneStudentFacultyFirst() {
        boolean exp = true;
        boolean act = oneStudentFaculty("2 faculty");
        assertEquals(exp, act);
    }

    @Test
    public void oneStudentFacultyNull() {
        boolean exp = false;
        boolean act = oneStudentFaculty(null);
        assertEquals(act,exp);
    }

    @Test
    public void oneStudentFacultyEmpty() {
        boolean exp = false;
        boolean act = oneStudentFaculty("");
        assertEquals(act, exp);
    }

    /*============================================================================*/
    /*============================= allStudentsGroup ============================*/
    /*============================================================================*/

    @Test
    public void allStudentsGroupFirst() {
        boolean exp = false;
        boolean act = allStudentsGroup("530");
        assertEquals(exp, act);
    }

    @Test
    public void allStudentsGroupNull() {
        boolean exp = false;
        boolean act = allStudentsGroup(null);
        assertEquals(act,exp);
    }

    @Test
    public void allStudentsGroupEmpty() {
        boolean exp = false;
        boolean act = allStudentsGroup("");
        assertEquals(act, exp);
    }

    /*============================================================================*/
    /*============================== oneStudentGroup =============================*/
    /*============================================================================*/

    @Test
    public void oneStudentGroupFirst() {
        boolean exp = true;
        boolean act = oneStudentGroup("530");
        assertEquals(exp, act);
    }

    @Test
    public void oneStudentGroupNull() {
        boolean exp = false;
        boolean act = oneStudentGroup(null);
        assertEquals(act,exp);
    }

    @Test
    public void oneStudentGroupEmpty() {
        boolean exp = false;
        boolean act = oneStudentGroup("");
        assertEquals(act, exp);
    }
}