package com.github.application;

public class Animals {

    private String name;

    private String state;

    public Animals(String name, String state) {
        this.name = name;
        this.state = state;
    }

    public void eat(String state){
        System.out.println("Animal eating");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
