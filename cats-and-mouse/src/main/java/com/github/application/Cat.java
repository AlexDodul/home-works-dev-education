package com.github.application;

public class Cat extends Animals {

    public Cat(String name, String state) {
        super(name, state);
    }

    @Override
    public void eat(String state) {
        System.out.println("The cat" + this.getName() + " ate the " + state + " mouse");
        if (state.equals("not envenomed")) {
            System.out.println("Cat " + this.getName() + " is Live\n");
        } else {
            System.out.println("Cat " + this.getName() + " is dead\n");
        }
    }
}
