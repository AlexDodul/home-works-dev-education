package com.github.application;

public class RunApplication {
    public static void main(String[] args) {

        Cat catTom = new Cat("Tom", "Is a live");
        Cat catCat = new Cat("Cat", "Is a live");
        Cat catJoi = new Cat("Joi", "Is a live");
        Cat catMeow = new Cat("Meow", "Is a live");
        Cat[] cats = new Cat[]{catTom, catCat, catJoi, catMeow};

        Mouse mouseJerry = new Mouse("Jerry", "not envenomed");
        Mouse mouseWhile = new Mouse("While", "envenomed");
        Mouse mouseBlack = new Mouse("Black", "not envenomed");
        Mouse mouseMouse = new Mouse("Mouse", "envenomed");
        Mouse[] mice = new Mouse[]{mouseJerry, mouseWhile, mouseBlack, mouseMouse};

        for (int i = 0; i < cats.length; i++) {
            cats[i].eat(mice[i].getState());
        }
    }
}
