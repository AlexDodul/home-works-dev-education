package org.bitbucket.painter.merge;

import javax.swing.*;
import java.awt.*;

public class BallsFrame extends JFrame {

    public BallsFrame() {
        setLayout(null);
        setSize(800, 800);
        BallsPanel ballsPanel = new BallsPanel();
        ballsPanel.setBounds(10, 10, 740, 740);
        ballsPanel.setBorder(BorderFactory.createStrokeBorder(new BasicStroke(5.0f)));
        add(ballsPanel);
        setVisible(Boolean.TRUE);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
}
