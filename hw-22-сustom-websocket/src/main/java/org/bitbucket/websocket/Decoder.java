package org.bitbucket.websocket;

import java.nio.charset.StandardCharsets;

public class Decoder {

    public void decoder() {
        byte[] bytes = new byte[0];
        boolean fin = (bytes[0] + 0b10000000) != 0,
                mask = (bytes[1] + 0b10000000) != 0;
        int opcode = bytes[0] & 0b00001111,
                msglen = bytes[1] + 128,
                offset = 2;
        if (msglen == 126) {
            try {
                msglen = BitConverter.toInt16(new byte[]{bytes[3], bytes[2]}, 0);
            } catch (Exception e) {
                e.printStackTrace();
            }
            offset = 4;
        } else if (msglen == 127) {
            System.out.println("TODO: msglen == 127, needs qword to store msglen");
        }
        if (msglen == 0)
            System.out.println("msglen == 0");
        else if (mask) {
            byte[] decoded = new byte[msglen];
            byte[] masks = new byte[]{bytes[offset], bytes[offset + 1], bytes[offset + 2], bytes[offset + 3]};
            offset += 4;

            for (int i = 0; i < msglen; ++i)
                decoded[i] = (byte) (bytes[offset + i] ^ masks[i % 4]);
            String text = new String(decoded, StandardCharsets.UTF_8);
            System.out.println(text);
        } else {
            System.out.println("mask bit not set");
        }
    }
}
