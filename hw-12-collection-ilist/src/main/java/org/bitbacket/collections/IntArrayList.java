package org.bitbacket.collections;

import org.bitbacket.collections.exeption.ListEmptyExeption;

public class IntArrayList implements IList {

    private int[] array = new int[0];


    @Override
    public void init(int[] init) {
        if (init == null) {
            this.array = new int[0];
        } else {
            this.array = new int[init.length];
            for (int i = 0; i < init.length; i++) {
                this.array[i] = init[i];
            }
        }
    }

    @Override
    public String toString() {
        int size = size();
        StringBuilder str = new StringBuilder();
        if (size == 0){
            return str.toString();
        }
        for (int i = 0; i < size - 1; i++) {
            str.append(this.array[i]).append(", ");
        }
        str.append(this.array[size - 1]);
        return str.toString();
    }

    @Override
    public void clear() {
        this.array = new int[0];
    }

    @Override
    public int size() {
        return this.array.length;
    }

    @Override
    public int[] toArray() {
        int size = size();
        int[] result = new int[size];
        for (int i = 0; i < size; i++) {
            result[i] = this.array[i];
        }
        return result;
    }

    @Override
    public void addStart(int val) {
        int size = size();
        int[] tmp = new int[size + 1];
        for (int i = 0; i < size; i++) {
            tmp[i + 1] = this.array[i];
        }
        tmp[0] = val;
        this.array = tmp;
    }

    @Override
    public void addEnd(int val) {
        int size = size();
        int[] tmp = new int[size + 1];
        for (int i = 0; i < size; i++) {
            tmp[i] = this.array[i];
        }
        tmp[size] = val;
        this.array = tmp;
    }

    @Override
    public void addByPos(int pos, int val) {
        int size = size();
        if (pos > size || pos < 0) {
            throw new ListEmptyExeption();
        }
        int[] tmp = new int[size + 1];
        for (int i = 0; i < pos; i++) {
            tmp[i] = this.array[i];
        }
        tmp[pos] = val;
        for (int i = pos + 1; i <= size; i++) {
            tmp[i] = this.array[i - 1];
        }
        this.array = tmp;
    }

    @Override
    public int removeStart() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyExeption();
        }
        int tmp = this.array[0];
        int[] result = new int[size - 1];
        for (int i = 0; i < size - 1; i++) {
            result[i] = this.array[i + 1];
        }
        return tmp;
    }

    @Override
    public int removeEnd() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyExeption();
        }
        int tmp = this.array[size - 1];
        int[] result = new int[size - 1];
        for (int i = 0; i < size - 1; i++) {
            result[i] = this.array[i];
        }
        return tmp;
    }

    @Override
    public int removeByPos(int pos) {
        int size = size();
        if (pos > size || pos < 0) {
            throw new ListEmptyExeption();
        }
        int result = this.array[pos];
        int[] tmp = new int[size - 1];
        for (int i = 0; i < pos; i++) {
            tmp[i] = this.array[i];
        }
        for (int i = pos; i < size - 1; i++) {
            tmp[i] = this.array[i + 1];
        }
        return result;
    }

    @Override
    public int max() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyExeption();
        }
        ;
        int result = this.array[0];
        for (int i = 0; i < this.array.length; i++) {
            if (result < this.array[i]) {
                result = this.array[i];
            }
        }
        return result;
    }

    @Override
    public int min() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyExeption();
        }
        int result = this.array[0];
        for (int i = 0; i < size; i++) {
            if (result > this.array[i]) {
                result = this.array[i];
            }
        }
        return result;
    }

    @Override
    public int maxPos() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyExeption();
        }
        int result = 0;
        for (int i = 1; i < size; i++) {
            if (this.array[i] > this.array[result]) {
                result = i;
            }
        }
        return result;
    }

    @Override
    public int minPos() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyExeption();
        }
        int result = 0;
        for (int i = 1; i < size; i++) {
            if (this.array[i] < this.array[result]) {
                result = i;
            }
        }
        return result;
    }

    @Override
    public int[] sort() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyExeption();
        }
        int temp;
        for (int i = 1; i < size; i++) {
            for (int j = i; j > 0; j--) {
                if (this.array[j] < this.array[j - 1]) {
                    temp = this.array[j];
                    this.array[j] = this.array[j - 1];
                    this.array[j - 1] = temp;
                }
            }
        }
        return this.array;
    }

    @Override
    public int get(int pos) {
        int size = size();
        if (size == 0) {
            throw new ListEmptyExeption();
        }
        if (pos < 0 || pos >= size) {
            throw new ListEmptyExeption();
        }
        return this.array[pos];
    }

    @Override
    public int[] halfRevers() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyExeption();
        }
        final int len = size / 2;
        final int offset = size - len;
        for (int i = 0; i < len; i++) {
            int temp = this.array[i];
            this.array[i] = this.array[offset + i];
            this.array[offset + i] = temp;
        }
        return this.array;
    }

    @Override
    public int[] revers() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyExeption();
        }
        int tmp;
        for (int i = 0; i < size / 2; i++) {
            tmp = this.array[i];
            this.array[i] = this.array[array.length - 1 - i];
            array[array.length - 1 - i] = tmp;
        }
        return this.array;
    }

    @Override
    public void set(int pos, int val) {
        int size = size();
        if (pos > size || pos < 0 || size == 0) {
            throw new ListEmptyExeption();
        }
        this.array[pos] = val;
    }
}