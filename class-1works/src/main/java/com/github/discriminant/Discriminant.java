package com.github.discriminant;

public class Discriminant {
    public static int discrim(int a, int b, int c){
        return b * b - 4 * a * c;
    }
}
