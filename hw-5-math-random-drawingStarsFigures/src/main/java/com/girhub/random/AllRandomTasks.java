package com.girhub.random;

import java.util.Arrays;

public class AllRandomTasks {
    public static void main(String[] args) {
        System.out.println("Task 1: " + randomNumber());
        System.out.println("Task 2: " + Arrays.toString(arr()));
        System.out.println("Task 3: " + Arrays.toString(arrayTen()));
        System.out.println("Task 4: " + Arrays.toString(arrayFromTwentyToFifty()));
        System.out.println("Task 5: " + Arrays.toString(arrayFromOddTen()));
        System.out.println("Task 6: " + Arrays.toString(randomAmountOfNumbers()));
    }

    // 1 Вывести на консоль случайное число.
    public static float randomNumber() {
        return (float) Math.random();
    }

    // 2 Вывести на консоль 10 случайных чисел.
    public static int[] arr() {
        int[] array = new int[10];
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 100);
        }
        return array;
    }

    // 3 Вывести на консоль 10 случайных чисел, каждое в диапазоне от 0 до 10.
    public static int[] arrayTen() {
        int[] array = new int[10];
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 10);
        }
        return array;
    }

    // 4 Вывести на консоль 10 случайных чисел, каждое в диапазоне от 20 до 50.
    public static int[] arrayFromTwentyToFifty() {
        int[] array = new int[10];
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) ((Math.random() * 30) + 20);
        }
        return array;
    }

    // 5 Вывести на консоль 10 случайных чисел, каждое в диапазоне от -10 до 10.
    public static int[] arrayFromOddTen() {
        int[] array = new int[10];
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) ((Math.random() * 20) - 10);
        }
        return array;
    }

    // 6 Вывести на консоль случайное количество (в диапазоне от 3 до 15) случайных чисел,
    // каждое в диапазоне от -10 до 35.
    public static int[] randomAmountOfNumbers() {
        int a = (int) ((Math.random() * 12) + 3);
        int[] array = new int[a];
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) ((Math.random() * 45) - 10);
        }
        return array;
    }
}
