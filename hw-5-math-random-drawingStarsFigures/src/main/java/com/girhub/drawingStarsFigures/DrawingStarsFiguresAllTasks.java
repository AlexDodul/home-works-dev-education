package com.girhub.drawingStarsFigures;

public class DrawingStarsFiguresAllTasks {
    public static void main(String[] args) {
        arrayStar1();
        arrayStar2();
        arrayStar3();
        arrayStar4();
        arrayStar5();
        arrayStar6();
        arrayStar7();
        arrayStar8();
        arrayStar9();
    }

    // 1
    public static void arrayStar1() {
        System.out.println("1 =============================");
        String[][] array = new String[7][7];
        for (int i = 0; i < array[0].length; i++) {
            for (int j = 0; j < array[1].length; j++) {
                System.out.print("*\t");
            }
            System.out.println();
        }
        System.out.println("===============================");
    }

    // 2
    public static void arrayStar2() {
        System.out.println("2 =============================");
        String[][] array = new String[7][7];
        for (int i = 0; i < array[0].length; i++) {
            for (int j = 0; j < array[1].length; j++) {
                if (i == 0 || j == 0 || i == 6 || j == 6) {
                    System.out.print("*\t");
                } else {
                    System.out.print("\t");
                }
            }
            System.out.println();
        }
        System.out.println("===============================");
    }

    // 3
    public static void arrayStar3() {
        System.out.println("3 =============================");
        String[][] array = new String[7][7];
        for (int i = 0; i < array[0].length; i++) {
            for (int j = 0; j < array[1].length; j++) {
                if (i == 0 || j == 0 || i == (array[1].length - j - 1))
                    System.out.print("*\t");
                else
                    System.out.print("\t");
            }
            System.out.println();
        }
        System.out.println("===============================");
    }

    // 4
    public static void arrayStar4() {
        System.out.println("4 =============================");
        String[][] array = new String[7][7];
        for (int i = 0; i < array[0].length; i++) {
            for (int j = 0; j < array[1].length; j++) {
                if (i == 6 || j == 0 || i == j)
                    System.out.print("*\t");
                else
                    System.out.print("\t");
            }
            System.out.println();
        }
        System.out.println("===============================");
    }

    // 5
    public static void arrayStar5() {
        System.out.println("5 =============================");
        String[][] array = new String[7][7];
        for (int i = 0; i < array[0].length; i++) {
            for (int j = 0; j < array[1].length; j++) {
                if (i == 6 || j == 6 || i == (array[1].length - j - 1))
                    System.out.print("*\t");
                else
                    System.out.print("\t");
            }
            System.out.println();
        }
        System.out.println("===============================");
    }

    // 6
    public static void arrayStar6() {
        System.out.println("6 =============================");
        String[][] array = new String[7][7];
        for (int i = 0; i < array[0].length; i++) {
            for (int j = 0; j < array[1].length; j++) {
                if (i == 0 || j == 6 || i == j)
                    System.out.print("*\t");
                else
                    System.out.print("\t");
            }
            System.out.println();
        }
        System.out.println("===============================");
    }

    // 7
    public static void arrayStar7() {
        System.out.println("7 =============================");
        String[][] array = new String[7][7];
        for (int i = 0; i < array[0].length; i++) {
            for (int j = 0; j < array[1].length; j++) {
                if (i == j || i == (array[1].length - j - 1))
                    System.out.print("*\t");
                else
                    System.out.print("\t");
            }
            System.out.println();
        }
        System.out.println("===============================");
    }

    // 8
    public static void arrayStar8() {
        System.out.println("8 =============================");
        String[][] array = new String[7][7];
        for (int i = 0; i < array[0].length; i++) {
            for (int j = 0; j < array[1].length; j++) {
                if (i > array[0].length / 2) {
                    break;
                }
                if (i == 0 || i == (array[1].length - j - 1) || i == j) {
                    System.out.print("*\t");
                } else {
                    System.out.print(" \t");
                }
            }
            System.out.println();
        }
        System.out.println("===============================");
    }

    // 9
    public static void arrayStar9() {
        System.out.println("8 =============================");
        String[][] array = new String[7][7];
        for (int i = 0; i < array[0].length; i++) {
            for (int j = 0; j < array[1].length; j++) {
                if (i < array[0].length / 2) {
                    break;
                }
                if (i == 6 || i == (array[1].length - j - 1) || i == j) {
                    System.out.print("*\t");
                } else {
                    System.out.print(" \t");
                }
            }
            System.out.println();
        }
        System.out.println("===============================");
    }
}
