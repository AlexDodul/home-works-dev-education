package com.girhub.math;

import org.junit.Assert;
import org.junit.Test;

import static java.lang.Float.NaN;

public class MathAllTasksTest {
    // 1 --------------------------------------------------------
    @Test
    public void distanceDeg() {
        double exp = 0;
        double act = MathAllTasks.distanceDeg(45, 90);
        Assert.assertEquals(act, exp, 90);
    }

    @Test
    public void distanceDegZero() {
        double exp = 0;
        double act = MathAllTasks.distanceDeg(45, 0);
        Assert.assertEquals(act, exp, 0);
    }

    @Test
    public void distanceDegMany() {
        double exp = 0;
        double act = MathAllTasks.distanceDeg(45, 350);
        Assert.assertEquals(act, exp, 0);
    }

    @Test
    public void distanceDegOdd() {
        double exp = 0;
        double act = MathAllTasks.distanceDeg(45, -30);
        Assert.assertEquals(act, exp, 0);
    }

    // 2 --------------------------------------------------------
    @Test
    public void distance() {
        int exp = 240;
        int act = MathAllTasks.distance(60, 60, 0, 2);
        Assert.assertEquals(act, exp);
    }

    @Test
    public void distanceZero() {
        int exp = 0;
        int act = MathAllTasks.distance(0, 0, 0, 0);
        Assert.assertEquals(act, exp);
    }

    @Test
    public void distanceOdd() {
        int exp = 0;
        int act = MathAllTasks.distance(45, 45, 0, -2);
        Assert.assertEquals(act, exp, 0);
    }

    @Test
    public void distanceMany() {
        int exp = 10100;
        int act = MathAllTasks.distance(500, 500, 100, 10);
        Assert.assertEquals(act, exp, 0);
    }

    // 3 --------------------------------------------------------
    @Test
    public void graphZero() {
        boolean exp = true;
        boolean act = MathAllTasks.graph(0, 0);
        Assert.assertEquals(act, exp);
    }

    @Test
    public void graph() {
        boolean exp = true;
        boolean act = MathAllTasks.graph(1.5f, 1.5f);
        Assert.assertEquals(act, exp);
    }

    @Test
    public void graphFalse() {
        boolean exp = false;
        boolean act = MathAllTasks.graph(2f, 1.5f);
        Assert.assertEquals(act, exp);
    }

    // 4 --------------------------------------------------------
    @Test
    public void mathExpressionZero() {
        double exp = 1;
        double act = MathAllTasks.mathExpression(0);
        Assert.assertEquals(act, exp, 0);
    }

    @Test
    public void mathExpression() {
        double exp = 4.453016476824533;
        double act = MathAllTasks.mathExpression(25);
        Assert.assertEquals(act, exp, 0);
    }

    @Test
    public void mathExpressionMany() {
        double exp = NaN;
        double act = MathAllTasks.mathExpression(-25);
        Assert.assertEquals(act, exp, 0);
    }
}
