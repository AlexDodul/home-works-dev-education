package com.girhub.math;

public class MathAllTasks {

    // 1. Стрельба из гаубицы. Дан угол возвышения ствола а и начальная скорость полёта снаряда. v км/ч.
    // Вычислить расстояние полёта снаряда. Реализовать решения для угла в градусах и в радианах.
    public static double distanceDeg(double v, double alpha) {
        if (alpha > 90 || alpha < 0) {
            return 0;
        } else {
            alpha = (alpha * (3.14 / 180));
            double L = (v * v * Math.sin(2 * alpha)) / 9.8;
            return L;
        }
    }

    // 2. Скорость первого автомобиля v1 км/ч, второго — v2 км/ч, расстояние между ними s км.
    // Какое расстояние будет между ними через t ч, если автомобили движутся в разные стороны?
    public static int distance(int v1, int v2, int s, int t) {
        if (t < 0) {
            return 0;
        } else {
            return ((v1 + v2) * t + s);
        }
    }

    // 3. Записать логическое выражение, принимающее значение 1, если точка лежит внутри заштрихованной
    // области, иначе — 0.
    public static boolean graph(float x, float y) {
        if ((x >= 0) && (y >= 1.5 * x - 1) && (y <= x) || (x <= 0) && (y >= -1.5 * x - 1) && (y <= -x)) {
            return true;
        } else {
            return false;
        }
    }

    // 4. Вычислить значение выражения
    public static double mathExpression(double x) {
        double z = (6 * Math.log(Math.sqrt(Math.exp(x + 1) + 2 * (Math.exp(x)) * Math.cos(x)))
                / Math.log(x - Math.exp(x + 1) * Math.sin(x))) + Math.abs(Math.cos(x) / Math.exp(Math.sin(x)));
        return z;
    }

}
