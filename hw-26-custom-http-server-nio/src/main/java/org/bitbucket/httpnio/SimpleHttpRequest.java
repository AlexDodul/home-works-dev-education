package org.bitbucket.httpnio;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SimpleHttpRequest {

    private final Map<String, String> header;

    public SimpleHttpRequest(SocketChannel client) throws IOException {
        this.header = parseRequest(getRequest(client));
    }

    public String getRequestType() {
        return this.header.get("TypeRequest:");
    }

    public String getBody() {
        return this.header.get("Body: ");
    }

    public String getRequest(SocketChannel client) throws IOException {
        ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
        String request = "";
        int count = 0;
        while ((count = client.read(byteBuffer)) > -1) {
            System.out.println(count);
            byte[] bytearray = byteBuffer.array();
            request = request + new String(bytearray);
            System.out.println("--1--" + request);
            byteBuffer.clear();
            if (count < 1024) {
                break;
            }
        }
        return request;
    }

    public static Map<String, String> parseRequest(String request) throws IOException {
        String[] strings = new String[2];
        Map<String, String> header = new HashMap<>();
        List<String> splitReq = Stream.of(request.split("\n"))
                .map(el -> new String(el))
                .collect(Collectors.toList());
        int count = 0;
        for (String str : splitReq) {
            count++;
            if (count == 1) {
                strings = str.split("", 2);
                header.put("TypeRequest", strings[0]);
            } else {
                if (str != null && !str.isEmpty() && !str.equals("\r")) {
                    strings = str.split("", 2);
                    header.put(strings[0], strings[1]);
                } else if (strings[0].equals("Content-Length") && str.equals("\r")) {
                    int index = Integer.parseInt(strings[1].trim());
                    String body = splitReq.get(splitReq.size() - 1).substring(0, index);
                    header.put("Body: ", body);
                } else if (str.equals("\r")) {
                    break;
                }
            }
        }
        System.out.println(request);
        return header;
    }
}

