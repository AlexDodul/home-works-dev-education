package com.github.calculator;

public class CalcMath {
    public static int calc(int a, int b, String op) {
        int result = 0;
        switch (op) {
            case "+":
                result = a + b;
                break;
            case "-":
                result = a - b;
                break;
            case "*":
                result = a * b;
                break;
            case "/":
                result = a / b;
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + op);
        }
        return result;
    }
}
