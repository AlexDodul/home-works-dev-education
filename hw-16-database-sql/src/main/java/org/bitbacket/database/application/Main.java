package org.bitbacket.database.application;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Main {
    public static void main(String[] args) {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            String url = "jdbc:mysql://localhost:3306/alex";
            String user = "root";
            String password = "";
            Connection con = DriverManager.getConnection(url, user, password);
            if (con != null) {
                System.out.println("Successful");
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }
}
