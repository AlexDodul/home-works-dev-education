package com.github.randomizer;

public class RandomizerDialog {
    static int min;
    static int max;

    public static void randomizer() {
        System.out.print("Введите минимальное число: ");
        while ((min = ScannerWrapper.getNumber()) < 1 || min > 500) {
            System.out.print("Введите минимальное число от 1 до 500: ");
        }

        System.out.print("Введите максимальное число: ");
        while ((max = ScannerWrapper.getNumber()) < 1 || max > 500) {
            System.out.print("Введите максимальное число от 1 до 500: ");
        }

        if (min > max) {
            int t = min;
            min = max;
            max = t;
        }

        System.out.println();
        System.out.println("Диапазон случайных чисел от " + min + " до " + max);
        System.out.println();

        int[] ramdomNumbers = new int[max - min];
        CommandDialog.commandsDialog(ramdomNumbers, min, max);
    }
}
