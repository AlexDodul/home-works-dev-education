package com.github.randomizer;

public class CommandDialog {
    public static void commandsDialog(int[] ramdomNumbers, int min, int max) {
        int count = 0;
        int step = 0;

        System.out.print("Введите 'generate' или 'exit' или 'help': ");

        while (true) {
            String word = ScannerWrapper.getCommand();
            if (word.equals("generate")) {
                boolean boolVariable = true;
                while (boolVariable) {
                    int a = MathRandom.generateRandomNumber(min, max);
                    for (int i = 0; i < ramdomNumbers.length; i++) {
                        if (ramdomNumbers[i] == a) {
                            count++;
                        }
                    }
                    if (count == 0) {
                        ramdomNumbers[step] = a;
                        System.out.println(ramdomNumbers[step]);
                        count = 0;
                        step++;
                        boolVariable = false;
                    } else if (count != 0 && ramdomNumbers[max - min - 1] == 0) {
                        count = 0;
                        boolVariable = true;
                    } else {
                        System.out.println("Уникальных чисел в выбранном диапазоне не осталось. Введите слово 'exit' или 'help'.");
                        boolVariable = false;
                    }
                }
            } else if (word.equals("help")) {
                System.out.println("При введении слова 'generate' Вы получаете результат - рандомное число в диапазоне от минимума до максимума.");
                System.out.println("При введении слова 'help' Вы получаете подсказки.");
                System.out.println("Для выхода из программы введите слово 'exit'.");
            } else if (word.equals("exit")) {
                System.out.println("Приложение больше не активно.");
                break;
            }
        }
    }
}
