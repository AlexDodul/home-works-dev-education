package com.github.randomizer;

public class MathRandom {
    public static int generateRandomNumber(int max, int min) {
        return (int) (Math.random() * (max - min)) + min;
    }
}
