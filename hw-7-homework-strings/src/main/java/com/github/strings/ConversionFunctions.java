package com.github.strings;

public class ConversionFunctions {

    /** Написать и протестировать функции преобразования:  */

    /**
     * целого  числа в строку
     */
    public static String numberToString(int a) {
        return Integer.toString(a);
    }

    /**
     * вещественного числа в строку
     */
    public static String realNumberToString(double a) {
        return Double.toString(a);
    }

    /**
     * строки в целое число
     */
    public static Integer stringToNumber(String str) {
        if (str == null || str.isEmpty()) {
            return null;
        } else {
            return Integer.parseInt(str);
        }
    }

    /**
     * строки в вещественное число
     */
    public static Double stringToRealNumber(String str) {
        if (str == null || str.isEmpty()) {
            return null;
        } else {
            return Double.parseDouble(str);
        }
    }
}
