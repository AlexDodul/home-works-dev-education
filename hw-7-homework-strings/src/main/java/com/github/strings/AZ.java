package com.github.strings;

/**
 * 1. Вывести в одну строку символы:
 */

public class AZ {
    public static void main(String[] args) {
        System.out.println(stringAZ());
        charsAZ();
        System.out.println(stringZa());
        charsZa();
        System.out.println(stringRusAlp());
        charsRus();
        numbers();
        tablASCII();
    }

    /**
     * английского алфавита от ‘A’ до ‘Z’
     */
    public static String stringAZ() {
        String alphabet = "abcdefghijklmnopqrstuvwxyz";
        return alphabet.toUpperCase();
    }

    public static void charsAZ() {
        for (char i = 'A'; i <= 'Z'; i++) {
            System.out.print(i + " ");
        }
        System.out.println();
    }

    /**
     * английского алфавита от ‘z’ до ‘a’
     */
    public static String stringZa() {
        String alphabet = "abcdefghijklmnopqrstuvwxyz";
        StringBuilder stringBuilder = new StringBuilder(alphabet);
        return stringBuilder.reverse().toString();
    }

    public static void charsZa() {
        for (char i = 'z'; i >= 'a'; i--) {
            System.out.print(i + " ");
        }
        System.out.println();
    }

    /**
     * русского алфавита от ‘а’ до ‘я’
     */
    public static String stringRusAlp() {
        return "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
    }

    public static void charsRus() {
        for (char i = 'а'; i <= 'я'; i++) {
            System.out.print(i + " ");
        }
        System.out.println();
    }

    /**
     * цифры от ‘0’ до ‘9’
     */
    public static void numbers() {
        for (char i = '0'; i <= '9'; i++) {
            System.out.print(i + " ");
        }
        System.out.println();
    }

    /**
     * печатного диапазона таблицы ASCII
     */
    public static void tablASCII() {
        for (char i = 32; i <= 127; i++) {
            char c = i;
            System.out.print(c + " ");
        }
        System.out.println();
    }
}
