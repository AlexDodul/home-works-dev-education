package com.github.strings;

import java.util.Arrays;
import java.util.HashSet;

public class StringFunctions {

    /**
     * 3. Написать и протестировать функции работы со строками:
     */

    /**
     * 1 Дана строка, состоящая из слов, разделенных пробелами и знаками препинания. Определить длину самого короткого слова.
     */
    public static int minLineLength(String string) {
        if (string == null || string.isEmpty()) {
            return 0;
        }
        String str = string.replaceAll("\\p{Punct}", "");
        String[] arr = str.split(" ");

        int lengthWord = arr[0].length();
        for (int i = 1; i < arr.length; i++) {
            if (arr[i].length() < lengthWord) {
                lengthWord = arr[i].length();
            }
        }
        return lengthWord;
    }

    /**
     * 2 Дан массив слов. Заменить последние три символа слов, имеющих заданную длину на символ "$"
     */
    public static String[] array(String[] words) {
        if (words == null || words.equals("")) {
            return null;
        }
        String[] arr = new String[words.length];
        for (int i = 0; i < words.length; i++) {
            StringBuilder stringBuilder = new StringBuilder(words[i]);
            arr[i] = stringBuilder.replace(words[i].length() - 3, words[i].length(), "$$$").toString();
            //arr[i] = words[i].substring(words[i].length() - (words[i].length() - 3)).concat("$$$");  //Второй способ
        }
        return arr;
    }

    /**
     * 3 Добавить в строку пробелы после знаков препинания, если они там отсутствуют.
     */
    public static String space(String str) {
        if (str == null || str.equals("")) {
            return null;
        }
        return str.trim().replaceAll("\\s+", " ").replaceAll("(?<=\\p{Punct})(?=\\w)", " ");
    }

    /**
     * 4 Оставить в строке только один экземпляр каждого встречающегося символа
     */
    public static String oneCopy(String str) {
        if (str == null || str.equals("")) {
            return null;
        }
        int charLength = str.length();
        String result = "";
        for (int i = 0; i < charLength; i++) {
            if (!result.contains(str.charAt(i) + ""))
                result += str.charAt(i);
        }
        return result;
    }


    /**
     * 5 Подсчитать количество слов во введенной пользователем строке.
     */
    public static int countWords(String str) {
        if (str == null || str.equals("")) {
            return 0;
        }
        String[] arr = str.split(" ");
        return arr.length;
    }

    /**
     * 6 Удалить из строки ее часть с заданной позиции и заданной длины.
     */
    public static String deleteString(String str, int startIndex, int endIndex) {
        if (str == null || str.equals("")) {
            return null;
        } else if (str.length() < startIndex || str.length() < endIndex) {
            return null;
        } else {
            String s = str.substring(startIndex, endIndex);
            return str.replaceAll(s, "");
        }
    }

    /**
     * 7 Перевернуть строку, т.е. последние символы должны стать первыми, а первые последними.
     */
    public static String revers(String str) {
        if (str == null || str.equals("")) {
            return null;
        }
        StringBuilder stringBuilder = new StringBuilder(str);
        return stringBuilder.reverse().toString();
    }

    /**
     * 8 В заданной строке удалить последнее слово
     */
    public static String deleteLastWord(String str) {
        if (str == null || str.equals("")) {
            return null;
        }
        String[] arr = str.split(" ");
        String result = "";
        for (int i = 0; i < arr.length - 1; i++) {
            result += arr[i] + " ";
        }
        return result;
    }
}
