package com.github.strings;

import org.junit.Assert;
import org.junit.Test;

public class StringFunctionsTest {

    /**
     * 1 Дана строка, состоящая из слов, разделенных пробелами и знаками препинания. Определить длину самого короткого слова.
     */
    @Test
    public void minLineLength() {
        int exp = 1;
        int act = StringFunctions.minLineLength("Hello Java! I am happy");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void minLineLengthLittle() {
        int exp = 1;
        int act = StringFunctions.minLineLength("I");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void minLineLengthMany() {
        int exp = 2;
        int act = StringFunctions.minLineLength("regular expression, specified as string, must first be compiled into an instance of this class");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void minLineLengthEmpty() {
        int exp = 0;
        int act = StringFunctions.minLineLength("");
        Assert.assertEquals(exp, act);
    }

    /**
     * 2 Дан массив слов. Заменить последние три символа слов, имеющих заданную длину на символ "$"
     */
    @Test
    public void array() {
        String[] exp = {"aa$$$", "ss$$$", "ffff$$$", "wwwww$$$", "eeee$$$", "$$$"};
        String[] act = StringFunctions.array(new String[]{"aaaaa", "sssss", "fffffff", "wwwwwwww", "eeeeeee", "qqq"});
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void arrayLittle() {
        String[] exp = {"$$$"};
        String[] act = StringFunctions.array(new String[]{"qqq"});
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void arrayMany() {
        String[] exp = {"aa$$$", "ss$$$", "ffff$$$", "wwwww$$$", "eeee$$$", "$$$", "aa$$$", "ss$$$", "ffff$$$", "wwwww$$$", "eeee$$$", "$$$"};
        String[] act = StringFunctions.array(new String[]{"aaaaa", "sssss", "fffffff", "wwwwwwww", "eeeeeee", "qqq", "aaaaa", "sssss", "fffffff", "wwwwwwww", "eeeeeee", "qqq"});
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void arrayNull() {
        String[] act = StringFunctions.array(null);
        Assert.assertNull(act);
    }

    /**
     * 3 Добавить в строку пробелы после знаков препинания, если они там отсутствуют.
     */
    @Test
    public void space() {
        String exp = "Hello, Java!";
        String act = StringFunctions.space("Hello,Java!");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void spaceLittle() {
        String exp = "Hello, U";
        String act = StringFunctions.space("Hello,U");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void spaceMany() {
        String exp = "regular expression, specified as string, must: first! be compiled. into an! instance of this class";
        String act = StringFunctions.space("regular expression,specified as string,must:first!be compiled.into an!instance of this class");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void spaceNull() {
        String act = StringFunctions.space(null);
        Assert.assertNull(act);
    }

    @Test
    public void spaceEmpty() {
        String act = StringFunctions.space("");
        Assert.assertEquals(null, act);
    }

    /**
     * 4 Оставить в строке только один экземпляр каждого встречающегося символа
     */
    @Test
    public void oneCopy() {
        String exp = "Helo,Jav!";
        String act = StringFunctions.oneCopy("Hello,Java!");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void oneCopyLittle() {
        String exp = "Helo";
        String act = StringFunctions.oneCopy("Hello");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void oneCopyMany() {
        String exp = "regula xpsion,cfdt";
        String act = StringFunctions.oneCopy("regular expression,specified as string");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void oneCopyNull() {
        String act = StringFunctions.oneCopy(null);
        Assert.assertNull(act);
    }

    @Test
    public void oneCopyEmpty() {
        String act = StringFunctions.oneCopy("");
        Assert.assertEquals(null, act);
    }

    /**
     * 5 Подсчитать количество слов во введенной пользователем строке.
     */
    @Test
    public void countWords() {
        int exp = 5;
        int act = StringFunctions.countWords("Hello Java! I am happy");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void countWordsLittle() {
        int exp = 1;
        int act = StringFunctions.minLineLength("I");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void countWordsMany() {
        int exp = 15;
        int act = StringFunctions.countWords("regular expression, specified as string, must first be compiled into an instance of this class");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void countWordsEmpty() {
        int exp = 0;
        int act = StringFunctions.countWords("");
        Assert.assertEquals(exp, act);
    }

    /**
     * 6 Удалить из строки ее часть с заданной позиции и заданной длины.
     */
    @Test
    public void deleteString() {
        String exp = "Helava!";
        String act = StringFunctions.deleteString("Hello,Java!", 3, 7);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void deleteStringLittle() {
        String exp = "Hlo";
        String act = StringFunctions.deleteString("Hello", 1, 3);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void deleteStringMany() {
        String exp = "regular expecified as string";
        String act = StringFunctions.deleteString("regular expression,specified as string", 10, 20);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void deleteStringNull() {
        String act = StringFunctions.deleteString(null, 1, 2);
        Assert.assertNull(act);
    }

    @Test
    public void deleteStringEmpty() {
        String act = StringFunctions.deleteString("", 2, 3);
        Assert.assertEquals(null, act);
    }

    @Test
    public void deleteStringError() {
        String act = StringFunctions.deleteString("Hello", 10, 15);
        Assert.assertEquals(null, act);
    }

    /**
     * 7 Перевернуть строку, т.е. последние символы должны стать первыми, а первые последними.
     */
    @Test
    public void revers() {
        String exp = "!avaJ,olleH";
        String act = StringFunctions.revers("Hello,Java!");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void reversLittle() {
        String exp = "olleH";
        String act = StringFunctions.revers("Hello");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void reversMany() {
        String exp = "gnirts sa deificeps,noisserpxe raluger";
        String act = StringFunctions.revers("regular expression,specified as string");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void reversNull() {
        String act = StringFunctions.revers(null);
        Assert.assertNull(act);
    }

    @Test
    public void reversEmpty() {
        String act = StringFunctions.revers("");
        Assert.assertEquals(null, act);
    }

    /**
     * 8 В заданной строке удалить последнее слово
     */
    @Test
    public void deleteLastWord() {
        String exp = "Hello, ";
        String act = StringFunctions.deleteLastWord("Hello, Java!");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void deleteLastWordLittle() {
        String exp = "";
        String act = StringFunctions.deleteLastWord("Hello");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void rdeleteLastWordMany() {
        String exp = "regular expression,specified as ";
        String act = StringFunctions.deleteLastWord("regular expression,specified as string");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void deleteLastWordNull() {
        String act = StringFunctions.deleteLastWord(null);
        Assert.assertNull(act);
    }

    @Test
    public void deleteLastWordEmpty() {
        String act = StringFunctions.deleteLastWord("");
        Assert.assertEquals(null, act);
    }
}
