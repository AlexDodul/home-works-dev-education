package com.github.strings;

import org.junit.Assert;
import org.junit.Test;

public class ConversionFunctionsTest {

    // === Тесты на целое  число в строку ===
    @Test
    public void numberToString() {
        String exp = "5";
        String act = ConversionFunctions.numberToString(5);
        Assert.assertEquals(act, exp);
    }

    @Test
    public void numberToStringZero() {
        String exp = "0";
        String act = ConversionFunctions.numberToString(0);
        Assert.assertEquals(act, exp);
    }

    @Test
    public void numberToStringMany() {
        String exp = "1986464545";
        String act = ConversionFunctions.numberToString(1986464545);
        Assert.assertEquals(act, exp);
    }

    @Test
    public void numberToStringOdd() {
        String exp = "-1986464545";
        String act = ConversionFunctions.numberToString(-1986464545);
        Assert.assertEquals(act, exp);
    }

    // === Тесты на вещественное число в строку ===
    @Test
    public void realNumberToString() {
        String exp = "569.253";
        String act = ConversionFunctions.realNumberToString(569.253D);
        Assert.assertEquals(act, exp);
    }

    @Test
    public void realNumberToStringZero() {
        String exp = "0.0";
        String act = ConversionFunctions.realNumberToString(0D);
        Assert.assertEquals(act, exp);
    }

    @Test
    public void realNumberToStringMany() {
        String exp = "19864.05569";
        String act = ConversionFunctions.realNumberToString(19864.05569D);
        Assert.assertEquals(act, exp);
    }

    @Test
    public void realNumberToStringOdd() {
        String exp = "-198.569845";
        String act = ConversionFunctions.realNumberToString(-198.569845D);
        Assert.assertEquals(act, exp);
    }

    // === Тесты на строки в целое число ===
    @Test
    public void stringToNumber() {
        int exp = 569;
        int act = ConversionFunctions.stringToNumber("569");
        Assert.assertEquals(act, exp);
    }

    @Test
    public void stringToNumberZero() {
        int exp = 0;
        int act = ConversionFunctions.stringToNumber("0");
        Assert.assertEquals(act, exp);
    }

    @Test
    public void stringToNumberMany() {
        int exp = 56972213;
        int act = ConversionFunctions.stringToNumber("56972213");
        Assert.assertEquals(act, exp);
    }

    @Test
    public void stringToNumberOdd() {
        int exp = -69;
        int act = ConversionFunctions.stringToNumber("-69");
        Assert.assertEquals(act, exp);
    }

    @Test
    public void stringToNumberNull() {
        Integer act = ConversionFunctions.stringToNumber("");
        Assert.assertNull(act);
    }

    // === Тесты на строки в вещественное число ===
    @Test
    public void stringToRealNumber() {
        Double exp = 569.265;
        Double act = ConversionFunctions.stringToRealNumber("569.265");
        Assert.assertEquals(act, exp);
    }

    @Test
    public void stringToRealNumberZero() {
        Double exp = 0.0;
        Double act = ConversionFunctions.stringToRealNumber("0");
        Assert.assertEquals(act, exp);
    }

    @Test
    public void stringToRealNumberMany() {
        Double exp = 5697.25698;
        Double act = ConversionFunctions.stringToRealNumber("5697.25698");
        Assert.assertEquals(act, exp);
    }

    @Test
    public void stringToRealNumberOdd() {
        Double exp = -69.23;
        Double act = ConversionFunctions.stringToRealNumber("-69.23");
        Assert.assertEquals(act, exp);
    }

    @Test
    public void stringToRealNumberNull() {
        Double act = ConversionFunctions.stringToRealNumber("");
        Assert.assertNull(act);
    }
}
