package org.bitbucket.app.classwork;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class CustomHttpServerRun {
    public static void main(String[] args) throws IOException {

        ServerSocket serverSocket = new ServerSocket(8080);

        System.out.println("Server start! Port " + 8080);

        while (true){
            Socket socket = serverSocket.accept();
            InputStream in = socket.getInputStream();
            OutputStream out = socket.getOutputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(out));
            String str = "";

            int k = 0;
            while ((str = br.readLine()) != null && !str.isEmpty()){
                int i = str.indexOf("Countent-length");
                if (i != -1){
                    k = Integer.parseInt(str.substring(i + 16));
                }
                System.out.println(str);
            }

            if (k != -1){
                System.out.println(k);
            }

            bw.write("HTTP/1.1 200 OK\r\n" +
                    "Content-Length: 11\r\n" +
                    "Content-Type: text/html\r\n");

            bw.write("\r\n12345678910\r\n\r\n");

            System.out.println("12345678910");
            System.out.println("Socket closed!");

            bw.close();
            br.close();
            socket.close();
        }

    }
}
