package org.bitbucket.balls;

import javax.swing.*;
import java.awt.*;
import java.util.Random;

public class Ball extends JPanel implements Runnable {

    private Random random = new Random();

    private int dx;

    private int dy;

    private Color color;

    private Point point;

    int ballSize = (int) (100 * Math.random());

    int r = (int) (255 * Math.random());
    int g = (int) (255 * Math.random());
    int b = (int) (255 * Math.random());

    public Ball(Point point) {
        this.point = point;
        this.color = new Color(r, g, b);
        this.dx = this.random.nextInt(5) - 2;
        this.dy = this.random.nextInt(5) - 2;
        setSize(ballSize + 5, ballSize + 5);
        setOpaque(Boolean.FALSE);
    }

    private void move() {
        JPanel pan = (JPanel) getParent();
        if (this.point.x <= 0 || this.point.x + ballSize >= pan.getWidth()) {
            dx = -dx;
        }
        if (this.point.y <= 0 || this.point.y + ballSize >= pan.getHeight()) {
            dy = -dy;
        }
        this.point.translate(dx, dy);
        setLocation(point);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(color);
        g2d.fillOval(1, 1, ballSize, ballSize);
    }

    @Override
    public void run() {
        try {
            while (true) {
                move();
                Thread.sleep(10);
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
}
