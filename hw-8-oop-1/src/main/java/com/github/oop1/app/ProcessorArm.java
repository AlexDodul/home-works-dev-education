package com.github.oop1.app;

import java.util.Objects;

public class ProcessorArm extends Processor {

    private final String ARCHITECTURE = "ARM";

    public ProcessorArm(float frequency, float cache, int bitCapacity) {
        super(frequency, cache, bitCapacity);
    }

    @Override
    String dataProcess(String data) {
        return data.toUpperCase();
    }

    @Override
    String dataProcess(long data) {
        return Long.toString(data).toUpperCase();
    }

    @Override
    public String toString() {
        return "ProcessorArm{" +
                "ARCHITECTURE='" + ARCHITECTURE + '\'' +
                "} " + super.toString();
    }
}
