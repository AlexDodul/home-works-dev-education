package com.github.oop1.app;

import java.util.ArrayList;
import java.util.List;

public class Filter {

    public static Device[] filterAmr(Device[] devices) {
        List<Device> list = new ArrayList<>();
        for (Device device : devices) {
            if (device.getProcessor() instanceof ProcessorArm) {
                list.add(device);
            }
        }
        Device[] result = new Device[list.size()];
        return list.toArray(result);
    }

    public Device[] filterX86(Device[] devices) {
        List<Device> list = new ArrayList<>();
        for (Device device : devices) {
            if (device.getProcessor() instanceof ProcessorX86) {
                list.add(device);
            }
        }
        Device[] result = new Device[list.size()];
        return list.toArray(result);
    }

    public Device[] filterFrequency(Device[] devices, float frequency) {
        List<Device> list = new ArrayList<>();
        for (Device device : devices) {
            if (device.getProcessor().getFrequency() == frequency) {
                list.add(device);
            }
        }
        Device[] result = new Device[list.size()];
        return list.toArray(result);
    }

    public Device[] filterCache(Device[] devices, float cache) {
        List<Device> list = new ArrayList<>();
        for (Device device : devices) {
            if (device.getProcessor().getCache() == cache) {
                list.add(device);
            }
        }
        Device[] result = new Device[list.size()];
        return list.toArray(result);
    }

    public Device[] filterBitCapacity(Device[] devices, int bitCapacity) {
        List<Device> list = new ArrayList<>();
        for (Device device : devices) {
            if (device.getProcessor().getBitCapacity() == bitCapacity) {
                list.add(device);
            }
        }
        Device[] result = new Device[list.size()];
        return list.toArray(result);
    }

    public Device[] filterMemoryMore(Device[] devices, int memorySize) {
        List<Device> list = new ArrayList<>();
        for (Device device : devices) {
            if (device.getMemory().getMemorySize() > memorySize) {
                list.add(device);
            }
        }
        Device[] result = new Device[list.size()];
        return list.toArray(result);
    }

    public Device[] filterMemoryLess(Device[] devices, int memorySize) {
        List<Device> list = new ArrayList<>();
        for (Device device : devices) {
            if (device.getMemory().getMemorySize() < memorySize) {
                list.add(device);
            }
        }
        Device[] result = new Device[list.size()];
        return list.toArray(result);
    }

    public Device[] filterUsedMemoryMore(Device[] devices, int memoryUsed) {
        List<Device> list = new ArrayList<>();
        for (Device device : devices) {
            if (device.getMemory().getMemoryInfo().getMemorySizePercent() > memoryUsed) {
                list.add(device);
            }
        }
        Device[] result = new Device[list.size()];
        return list.toArray(result);
    }

    public Device[] filterUsedMemoryLess(Device[] devices, int memoryUsed) {
        List<Device> list = new ArrayList<>();
        for (Device device : devices) {
            if (device.getMemory().getMemoryInfo().getMemorySizePercent() < memoryUsed) {
                list.add(device);
            }
        }
        Device[] result = new Device[list.size()];
        return list.toArray(result);
    }
}
