package com.github.oop1.app;

import java.util.ArrayList;
import java.util.List;

public class Device {

    private Processor processor;

    private Memory memory;

    public Device(Processor processor, int memorySize) {
        this.processor = processor;
        this.memory = new Memory(memorySize);
    }

    public Processor getProcessor() {
        return processor;
    }

    public Memory getMemory() {
        return memory;
    }

    public void save(String[] data) {
        for (String str : data) {
            memory.save(str);
        }
    }

    public String[] readAll() {
        List<String> list = new ArrayList<>();
        list.add(memory.readLast());
        memory.removeLast();
        String[] result = new String[list.size()];
        result = list.toArray(result);
        return result;
    }

    public void dataProcessing() {
    }

    public String getSystemInfo() {
        return processor.getDetails() + memory.getMemoryInfo().toString();
    }
}
