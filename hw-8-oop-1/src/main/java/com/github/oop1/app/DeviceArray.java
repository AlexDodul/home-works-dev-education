package com.github.oop1.app;

public class DeviceArray {

    public static Device[] devices = new Device[10];

    static {
        devices[0] = new Device(new ProcessorX86(2.9f, 1, 16), 1024);
        devices[1] = new Device(new ProcessorX86(2.5f, 1, 32), 2048);
        devices[2] = new Device(new ProcessorX86(3.6f, 1, 64), 4096);
        devices[3] = new Device(new ProcessorX86(3.3f, 1, 32), 8192);
        devices[4] = new Device(new ProcessorX86(3.5f, 1, 16), 16384);
        devices[5] = new Device(new ProcessorArm(2.2f, 1, 64), 16384);
        devices[6] = new Device(new ProcessorArm(2.2f, 1, 64), 8192);
        devices[7] = new Device(new ProcessorArm(3.0f, 1, 64), 4096);
        devices[8] = new Device(new ProcessorArm(3.3f, 1, 64), 2048);
        devices[9] = new Device(new ProcessorArm(3.1f, 1, 64), 1024);
    }
}
