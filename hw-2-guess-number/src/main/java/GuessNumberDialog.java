public class GuessNumberDialog {
    static int x;
    static int min;
    static int max;

    public static void guestNumber() {
        System.out.print("Введите минимальное (min) число: ");
        while ((min = ScannerWrapper.getNumber()) < 1 || min > 200) {
            System.out.println("Допустимый диапазон минимального числа от 1 до 200");
            System.out.print("Введите минимальное (min) число: ");
        }

        System.out.println();
        System.out.print("Введите максимальное (max) число: ");
        while ((max = ScannerWrapper.getNumber()) < 1 || max > 200) {
            System.out.println("Допустимый диапазон максимального числа от 1 до 200");
            System.out.print("Введите максимальное (max) число: ");
        }

        if (min > max) {
            int t = min;
            min = max;
            max = t;
        }
        System.out.print("Введите количество попыток от 1 до 15: ");
        while ((x = ScannerWrapper.getNumber()) < 1 || x > 15) {
            System.out.print("Введите количество попыток от 1 до 15: ");
        }
        CalculationGuestNumber.calculationGuestNumber(min, max, x);
    }

}
