import java.util.Scanner;

public class ScannerWrapper {
    public static final Scanner SCANNER = new Scanner(System.in);

    public static int getNumber() {
        return SCANNER.nextInt();
    }

    public static String getLine() {
        return SCANNER.nextLine();
    }
}
