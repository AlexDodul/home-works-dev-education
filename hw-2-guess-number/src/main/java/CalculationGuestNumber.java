public class CalculationGuestNumber {

    public static void calculationGuestNumber(int min, int max, int x) {
        System.out.println("Привет, я загадал число от " + min + " до " + max + " вашего диапазона. Попробуй угадать его за " + x + " попыток!\n");
        int generationNumber = MathRandom.generateRandomNumber(min, max);

        //System.out.println("================ " + generationNumber + " ========================");

        System.out.println("Ваша первая попрытка: ");

        boolean bool = true;
        int youTry = 0;
        int count = x;
        int n = 1;
        String exit = "";

        youTry = ScannerWrapper.getNumber();
        int firstNumber = youTry;

        if (generationNumber == youTry) {
            System.out.println("Поздравляю! Ты угадал задуманное число за " + n + " попыток");
        } else if (generationNumber != youTry) {
            System.out.println("Не угадал!!!");
            while (bool) {
                if (ScannerWrapper.SCANNER.hasNextInt()) {
                    youTry = ScannerWrapper.getNumber();
                    n++;
                    if (generationNumber == youTry) {
                        System.out.println("Поздравляю! Ты угадал задуманное число за " + n + " попыток");
                        bool = false;
                    } else if (generationNumber != youTry && x >= n) {
                        count--;
                        if (firstNumber < youTry && youTry < generationNumber) {
                            System.out.println("Не угадал, но теплее!!! Осталось " + count + " попытки");
                        } else if (firstNumber > youTry && youTry > generationNumber) {
                            System.out.println("Не угадал, но теплее!!! Осталось " + count + " попытки");
                        } else {
                            System.out.println("Не угадал, холоднее... Осталось " + count + " попытки");
                        }
                        firstNumber = youTry;
                        bool = true;
                    } else if (generationNumber != youTry && x < n) {
                        System.out.println("Ты проиграл");
                        bool = false;
                    }
                } else if (ScannerWrapper.SCANNER.hasNextLine()) {
                    exit = ScannerWrapper.getLine();
                    if (exit.equals("exit")) {
                        System.out.println("Программа завершена!");
                        bool = false;
                    }
                }

            }
        }
    }

}
