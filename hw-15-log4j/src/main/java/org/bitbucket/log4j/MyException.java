package org.bitbucket.log4j;

public class MyException extends RuntimeException{
    public MyException(String message){
        super(message);
    }
}
