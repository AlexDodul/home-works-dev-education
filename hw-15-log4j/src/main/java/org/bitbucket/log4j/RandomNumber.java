package org.bitbucket.log4j;

import org.apache.log4j.Logger;

public class RandomNumber {

    private int randomNumber;

    private static final Logger log = Logger.getLogger(String.valueOf(RandomNumber.class));

    public int getRandomNumber() {
        try {
            randomNumber = (int) (Math.random() * 10);
            if (randomNumber >= 5 ) {
                log.info("Приложение успешно запущено");
                return randomNumber;
            }
            throw new MyException("Сгенерированное число - " + randomNumber);
        } catch (MyException ex) {
            log.error(ex.getMessage());
            return randomNumber;
        }
    }
}
