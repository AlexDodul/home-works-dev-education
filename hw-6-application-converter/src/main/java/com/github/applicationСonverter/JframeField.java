package com.github.applicationСonverter;

import javax.swing.*;

public class JframeField{
    public void field() {
        JFrame frame = new JFrame("=== Converter ===");
        frame.setSize(1200, 800);
        frame.setLayout(null);

        JPanel lengthConverter = new LengthConverter().convert();
        frame.add(lengthConverter);

        JPanel timeConverter = new TimeConverter().convert();
        frame.add(timeConverter);

        JPanel weightConverter = new WeightConverter().convert();
        frame.add(weightConverter);

        JPanel volumeConverter = new VolumeConverter().convert();
        frame.add(volumeConverter);

        JPanel temperatureConverter = new TemperatureConverter().convert();
        frame.add(temperatureConverter);

        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
