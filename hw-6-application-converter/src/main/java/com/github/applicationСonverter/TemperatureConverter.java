package com.github.applicationСonverter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.HashMap;

public class TemperatureConverter {
    private JButton myButton;
    private JLabel titleLabel;
    private JLabel centerLabel;
    private JLabel bLabel;
    private JTextField frField;
    private JComboBox<String> fromBox;
    private JTextField toField;
    private JComboBox<String> toBox;
    private final String[] lengthTypes = {"C(Шкала Цельсия)", "K(Шкала Кельвина)","F(Шкала Фаренгейта)", "Re(Шкала Реомюра)", "Ro(Шкала Рёмер)", "Ra(Шкала Ранкина)", "N(Шкала Ньютона)", "D(Шкала Дели́ля)"};
    private final HashMap<String, HashMap<String, Double>> converter = new HashMap<>();

    public JPanel convert() {

        JPanel firstPanel = new JPanel();
        firstPanel.setLayout(null);
        firstPanel.setBounds(600, 400, 400, 400);
        firstPanel.setBackground(Color.GRAY);
        firstPanel.setVisible(true);

        titleLabel = new JLabel("=====Temperature CONVERTER=====");
        titleLabel.setBounds(85, 5, 250, 20);
        firstPanel.add(titleLabel);

        frField = new JTextField("0", 20);
        frField.setBounds(70, 35, 250, 30);
        firstPanel.add(frField);

        centerLabel = new JLabel("From:");
        centerLabel.setBounds(20, 85, 100, 30);
        firstPanel.add(centerLabel);

        fromBox = new JComboBox<>(lengthTypes);
        fromBox.setBounds(70, 85, 250, 30);
        firstPanel.add(fromBox);

        bLabel = new JLabel("To:");
        bLabel.setBounds(20, 130, 100, 30);
        firstPanel.add(bLabel);

        toBox = new JComboBox<>(lengthTypes);
        toBox.setBounds(70, 130, 250, 30);
        firstPanel.add(toBox);

        myButton = new JButton("CONVERT");
        myButton.setBounds(50, 200, 300, 60);
        firstPanel.add(myButton);
        myButton.addActionListener(new SubmitListener());

        toField = new JTextField(20);
        toField.setBounds(70, 280, 250, 50);
        toField.setEnabled(false);
        firstPanel.add(toField);

        Font labelFont = new Font("Arial", Font.BOLD, 20);
        toField.setFont(labelFont);

        return firstPanel;
    }

    public TemperatureConverter() {

        converter.put("C(Шкала Цельсия)", new HashMap<>());
        converter.get("C(Шкала Цельсия)").put("C(Шкала Цельсия)", 1.0);
        converter.get("C(Шкала Цельсия)").put("K(Шкала Кельвина)", 274.15);
        converter.get("C(Шкала Цельсия)").put("F(Шкала Фаренгейта)", 33.8);
        converter.get("C(Шкала Цельсия)").put("Re(Шкала Реомюра)", 0.7999999999999987);
        converter.get("C(Шкала Цельсия)").put("Ro(Шкала Рёмер)", 8.025);
        converter.get("C(Шкала Цельсия)").put("Ra(Шкала Ранкина)", 493.5);
        converter.get("C(Шкала Цельсия)").put("N(Шкала Ньютона)", 0.33);
        converter.get("C(Шкала Цельсия)").put("D(Шкала Дели́ля)", 148.5);

        converter.put("K(Шкала Кельвина)", new HashMap<>());
        converter.get("K(Шкала Кельвина)").put("K(Шкала Кельвина)", 1.0);
        converter.get("K(Шкала Кельвина)").put("C(Шкала Цельсия)", -272.1);
        converter.get("K(Шкала Кельвина)").put("F(Шкала Фаренгейта)", -457.9);
        converter.get("K(Шкала Кельвина)").put("Re(Шкала Реомюра)", -217.7);
        converter.get("K(Шкала Кельвина)").put("Ro(Шкала Рёмер)", -135.4);
        converter.get("K(Шкала Кельвина)").put("Ra(Шкала Ранкина)", 1.8);
        converter.get("K(Шкала Кельвина)").put("N(Шкала Ньютона)", -89.81);
        converter.get("K(Шкала Кельвина)").put("D(Шкала Дели́ля)", 558.2);

    }

    class SubmitListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            Double numToConvert = Double.parseDouble(frField.getText());
            Double conversionRate = converter.get(fromBox.getSelectedItem()).get(toBox.getSelectedItem());
            String result = new DecimalFormat("#0.000000000").format(numToConvert * conversionRate);
            toField.setText(result);

        }
    }
}
